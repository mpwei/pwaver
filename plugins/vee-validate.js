import { extend, localize } from 'vee-validate'
import { required, email, min, excluded } from 'vee-validate/dist/rules'
import TW from 'vee-validate/dist/locale/zh_TW.json'
import JP from 'vee-validate/dist/locale/ja.json'
import EN from 'vee-validate/dist/locale/en.json'

extend('email', email)
extend('excluded', excluded)
extend('required', required)

localize({
  'zh-tw': {
    messages: TW.messages
  },
  'en-us': {
    messages: EN.messages
  },
  'ja-jp': {
    messages: JP.messages
  }
})

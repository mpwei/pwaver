import axios from 'axios'

const request = axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? process.env.BASE_URL : 'http://localhost:3000'
})

export {
  axios,
  request
}

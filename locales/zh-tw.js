export default {
  navigation: {
    index: '首頁',
    '3DSpace': '3D空間模型',
    about: '關於我們',
    news: '最新消息',
    product: '產品與服務',
    download: '下載專區',
    contact: '聯絡我們'
  },
  index: {
    title: '首頁',
    fieldCaseTitle: '應用場域',
    customerTitle: '客戶'
  },
  contact: {
    title: '聯絡我們',
    formPlaceholder: {
      name: '姓名',
      email: '信箱',
      phone: '電話',
      message: '您的留言',
      send: '送出'
    }
  },
  global: {
    messages: {
      process: '資料處理中...',
      sendMailSuccess: '已送出訊息，請留意您的電子信箱與手機，我們將有專人為您服務。',
      sendMailError: '發信錯誤!!'
    },
    more: 'More',
    viewDetail: '查看詳情'
  },
  download: {
    downloadSystemMenu: {
      name: '系統應用說明'
    },
    downloadTestData: {
      name: '檢測數據報告'
    },
    downloadDetect: {
      name: '偵測儀器介紹'
    },
    button: '下載'
  },
  footer: {
    siteMap: '網站地圖',
    address: '地址',
    tel: '電話',
    fax: '傳真',
    email: '信箱',
    language: {
      zh: '中',
      en: '英',
      ja: '日'
    }
  }
}

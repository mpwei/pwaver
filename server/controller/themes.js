const admin = require('firebase-admin')
const db = admin.database()
const fs = admin.firestore()

module.exports = {
  async Carousel (req, res, next) {
    const Carousel = await db.ref('Carousel').once('value').then((r) => {
      return r.val()
    }).catch((e) => {
      return res.status(500).send({
        code: 500,
        msg: 'Get Carousel Error: ' + e
      })
    })
    const CarouselBottom = await db.ref('CarouselBottom').once('value').then((r) => {
      return r.val()
    }).catch((e) => {
      return res.status(500).send({
        code: 500,
        msg: 'Get CarouselBottom Error: ' + e
      })
    })
    return res.send({
      code: 200,
      msg: 'Success',
      Carousel,
      CarouselBottom
    })
  },
  async Location (req, res, next) {
    const data = await db.ref('ServiceLocation').once('value').then((r) => {
      return r.val()
    }).catch((e) => {
      return res.status(500).send({
        code: 500,
        msg: 'Get ServiceLocation Error: ' + e
      })
    })
    return res.send(data)
  },
  async Area (req, res, next) {
    const AllSchool = []
    await fs.collection('Location').get().then(querySnapshot => {
      querySnapshot.forEach(doc => {
        AllSchool.push(doc.data())
      })
    })
    return res.send(AllSchool)
  },
  async Contact (req, res, next) {
    const data = await db.ref('Contact').once('value').then((r) => {
      return r.val()
    }).catch((e) => {
      return res.status(500).send({
        code: 500,
        msg: 'Contact Error: ' + e
      })
    })
    return res.send(data)
  },
  async Index (req, res, next) {
    const data = await db.ref('Index').once('value').then((r) => {
      return r.val()
    }).catch((e) => {
      return res.status(500).send({
        code: 500,
        msg: 'Contact Error: ' + e
      })
    })
    return res.send(data)
  },
  async Meta (req, res, next) {
    const data = await db.ref('Info').once('value').then((r) => {
      return r.val()
    }).catch((e) => {
      return res.status(500).send({
        code: 500,
        msg: 'Get Info Error: ' + e
      })
    })
    return res.send(data)
  },
  async Social (req, res, next) {
    const data = await db.ref('Social').once('value').then((r) => {
      return r.val()
    }).catch((e) => {
      return res.status(500).send({
        code: 500,
        msg: 'Get Social Error: ' + e
      })
    })
    return res.send(data)
  }
}

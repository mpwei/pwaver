const express = require('express')

// Create express instance
const app = express()
app.use(express.json())

// Firebase admin
const admin = require('firebase-admin')
const serviceAccount = require('./cert/p-waver-firebase-adminsdk-bqpj1-83de555290.json')
if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://p-waver-default-rtdb.firebaseio.com',
    projectId: 'p-waver'
  })
}

// Require API routes
const route = require('./routes/index')
const themes = require('./routes/themes')
const message = require('./routes/message')

// Import API Routes
app.use('/', route)
app.use('/themes', themes)
app.use('/message', message)

// Export express app
module.exports = app

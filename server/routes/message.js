const crypto = require('crypto')
const { Router } = require('express')
const router = Router()
const Axios = require('axios')
const Cookies = require('cookie-universal')
const Date = require('dayjs')
const admin = require('firebase-admin')

const Request = Axios.create({
  baseURL: 'https://images.mpwei.tw/',
  timeout: 60000,
  headers: {
    'Content-Type': 'application/json'
  }
})

function encrypt (str) {
  return crypto.createHash('md5').update(str).digest('hex')
}

const SendMail = async function (_body, _visitToken) {
  let MailContent = '姓名: ' + _body.name + '<br>'
  MailContent += '電子郵件: ' + _body.email + '<br>'
  MailContent += '聯絡電話: ' + _body.phone + '<br>'
  MailContent += '留言內容: <br>' + _body.message

  const SystemMail = await admin.database().ref('Info/SystemEmail').once('value').then((result) => {
    return result.val() || null
  })

  if (SystemMail === null) {
    throw {
      code: 5001,
      message: 'SystemEmail is not set.'
    }
  }

  const SendData = {
    mail_from: 'notify@mpweistore.com',
    mail_from_name: 'P-Waver',
    mail_to: SystemMail,
    mail_title: '您有一筆新諮詢 - 來自' + _body.name,
    mail_content: MailContent
  }
  return Request.post('/action/service/sent_mail', {
    timestamp: Date().unix(),
    token: _body.token,
    visitToken: _visitToken,
    signature: encrypt('vBkSlQtXBNemF9e3' + JSON.stringify(SendData) + Date().unix()),
    data: SendData
  }).then((res) => {
    if (res.data.msg === 'success') {
      return true
    } else {
      throw res.data
    }
  }).catch((e) => {
    throw e
  })
}

router.get('/token', async function (req, res) {
  await Request.post('/action/service/token', {
    visitToken: Cookies(req, res).get('visitID')
  }).then(({ data }) => {
    if (data.msg === 'success') {
      return res.send(data.token)
    } else {
      // eslint-disable-next-line no-throw-literal
      throw 'Cannot get token.'
    }
  }).catch((e) => {
    throw e
  })
})

router.post('/notify/:type', async function (req, res) {
  let Action
  switch (req.params.type) {
    case 'mail':
    case 'newsletter':
    default:
      Action = SendMail(req.body, Cookies(req, res).get('visitID'))
      break
  }
  await Action.then(() => {
    return res.send({
      code: 200,
      msg: 'Success'
    })
  }).catch((e) => {
    return res.status(401).send({
      code: 401,
      msg: 'Fail: ' + e
    })
  })
})

module.exports = router

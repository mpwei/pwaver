const { Router } = require('express')
const router = Router()
const Axios = require('axios')

const Request = Axios.create({
  baseURL: 'https://mpwei-product-api-kxnw4gb5uq-de.a.run.app',
  timeout: 60000,
  headers: {
    'Content-Type': 'application/json'
  }
})

router.get('/', function (req, res, next) {
  return res.status(401).send({
    code: 401,
    msg: 'Forbidden'
  })
})

router.post('/category', (req, res) => {
  Request.post('/content/category/detail', {
    Project: 'PWAVER',
    Slug: req.body.Slug
  }).then(({ data }) => {
    return res.send({
      code: 200,
      msg: 'Success',
      Data: data.Data
    })
  }).catch((err) => {
    return res.status(501).send({
      code: 501,
      msg: 'Fail: ' + err
    })
  })
})

router.post('/article/list', (req, res) => {
  Request.post('/content/article/list', {
    Project: 'PWAVER',
    Categories: req.body.Categories,
    Tags: req.body.Tags,
    Page: req.body.Page ? req.body.Page : 1,
    PerPage: req.body.PerPage ? req.body.PerPage : 8,
    Order: req.body.Order ? req.body.Order : 'nto'
  }).then(({ data }) => {
    return res.send({
      Data: data.Data,
      Pagination: data.Pagination
    })
  }).catch((err) => {
    // eslint-disable-next-line no-console
    console.log(JSON.stringify(err))
    return res.status(501).send({
      code: 501,
      msg: 'Fail: ' + err
    })
  })
})

router.get('/article/detail/:slug', (req, res) => {
  Request.post('/content/article/detail', {
    Project: 'PWAVER',
    Slug: req.params.slug
  }).then(({ data }) => {
    return res.send(data)
  }).catch((err) => {
    // eslint-disable-next-line no-console
    console.log(JSON.stringify(err))
    return res.status(501).send({
      code: 501,
      msg: 'Fail: ' + err
    })
  })
})

module.exports = router

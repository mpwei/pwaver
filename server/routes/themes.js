const { Router } = require('express')
const router = Router()
const Themes = require('../controller/themes')

router.get('/carousel', Themes.Carousel)
router.get('/location', Themes.Location)
router.get('/contact', Themes.Contact)
router.get('/index', Themes.Index)
router.get('/meta', Themes.Meta)
router.get('/social', Themes.Social)

module.exports = router

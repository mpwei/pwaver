exports.ids = [12];
exports.modules = {

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Footer.vue?vue&type=template&id=3ec8688b&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.loadShow),expression:"loadShow"}],staticClass:"ri-footer"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"ri-footer__info\">","</div>",[_vm._ssrNode("<div class=\"ri-footer__info__logo\"><img"+(_vm._ssrAttr("src",_vm.$store.state.Meta.LanguageData[_vm.$i18n.locale].Favicon || _vm.$store.state.Meta.Favicon))+(_vm._ssrAttr("alt",_vm.$store.state.Meta.LanguageData[_vm.$i18n.locale].Title || _vm.$store.state.Meta.Title))+"></div> "),_vm._ssrNode("<div class=\"ri-footer__info__map\">","</div>",[_vm._ssrNode("<h4>"+_vm._ssrEscape(" "+_vm._s(_vm.$t('footer.siteMap')))+"</h4> "),_vm._ssrNode("<ul>","</ul>",[_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/aboutUS')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.about'))+"\n            ")])],1),_vm._ssrNode(" "),_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/news')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.news'))+"\n            ")])],1),_vm._ssrNode(" "),_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/productBefore')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.product'))+"\n            ")])],1),_vm._ssrNode(" "),_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/downloadSystemMenu')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.download'))+"\n            ")])],1),_vm._ssrNode(" "),_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/contact')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.contact'))+"\n            ")])],1),_vm._ssrNode(" <li><ul class=\"ri-footer__info__map__group\"><li><a href=\"javascript:void(0)\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.$t('footer.language.zh'))+"\n                ")+"</a></li> <li><a href=\"javascript:void(0)\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.$t('footer.language.en'))+"\n                ")+"</a></li></ul></li>")],2)],2),_vm._ssrNode(" <div class=\"ri-footer__info__contact\"><h4 class=\"d-none d-md-block\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.$t('navigation.contact'))+"\n        ")+"</h4> <h4 class=\"d-block d-md-none\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.Contact.CompanyName)+"\n        ")+"</h4> <ul><li class=\"ri-footer__info__contact__address\">"+_vm._ssrEscape("\n            "+_vm._s(_vm.$t('footer.address'))+": "+_vm._s(_vm.Contact.Address)+"\n          ")+"</li> <li><a href=\"tel:+886-2-6630-5187\">"+_vm._ssrEscape(_vm._s(_vm.$t('footer.tel'))+": "+_vm._s(_vm.Contact.Tel))+"</a></li> <li class=\"ri-footer__info__contact__email\"><a"+(_vm._ssrAttr("href",_vm.Contact.Email))+">"+_vm._ssrEscape(_vm._s(_vm.$t('footer.email'))+": "+_vm._s(_vm.Contact.Email))+"</a></li> <li>"+_vm._ssrEscape(_vm._s(_vm.$t('footer.fax'))+": "+_vm._s(_vm.Contact.Fax))+"</li></ul></div> <div class=\"ri-footer__info__media\"><ul><li><a"+(_vm._ssrAttr("href",_vm.InstagramLink))+" target=\"_blank\" class=\"ri-a\""+(_vm._ssrStyle(null,null, { display: (_vm.InstagramLink) ? '' : 'none' }))+"><span class=\"icon-instagram\"></span></a></li> <li><a"+(_vm._ssrAttr("href",_vm.TwitterLink))+" target=\"_blank\" class=\"ri-a\""+(_vm._ssrStyle(null,null, { display: (_vm.TwitterLink) ? '' : 'none' }))+"><span class=\"icon-twitter\"></span></a></li> <li><a"+(_vm._ssrAttr("href",_vm.FBLink))+" target=\"_blank\" class=\"ri-a\""+(_vm._ssrStyle(null,null, { display: (_vm.FBLink) ? '' : 'none' }))+"><span class=\"icon-facebook\"></span></a></li> <li><a href=\"/\" class=\"ri-a\"><span class=\"icon-web\"></span></a></li></ul></div>")],2),_vm._ssrNode(" <div class=\"ri-footer__copyright\"><p>"+_vm._ssrEscape("Copyright © "+_vm._s(_vm.$store.state.Meta.LanguageData[_vm.$i18n.locale].Title || _vm.$store.state.Meta.Title)+" All Rights Reserved.")+"</p></div>")],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/Footer.vue?vue&type=template&id=3ec8688b&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Footer.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Footervue_type_script_lang_js_ = ({
  data() {
    return {
      loadShow: false,
      FBLink: '',
      TwitterLink: '',
      InstagramLink: '',
      Contact: {}
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.LoadAllLanguage();
    }

  },

  mounted() {
    this.loadShow = true;
    this.LoadAllLanguage();
    this.FBLink = this.$store.state.Social.find(item => {
      return item.Name === 'Facebook';
    }).Link;
    this.TwitterLink = this.$store.state.Social.find(item => {
      return item.Name === 'Twitter';
    }).Link;
    this.InstagramLink = this.$store.state.Social.find(item => {
      return item.Name === 'Instagram';
    }).Link;
  },

  methods: {
    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    },

    LoadAllLanguage() {
      this.Contact = this.DetermineLanguage(this.$store.state.Contact);
    }

  }
});
// CONCATENATED MODULE: ./components/Footer.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Footervue_type_script_lang_js_ = (Footervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/Footer.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(87)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_Footervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "5182b2b0"
  
)

/* harmony default export */ var Footer = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(88);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("33164fa7", content, true, context)
};

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(78);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-footer{background-color:#3d3d3d}.ri-footer__info{display:flex;justify-content:space-between;color:#fff}@media (max-width:767.98px){.ri-footer__info{display:block}}.ri-footer__info__logo{flex-shrink:0;width:200px}@media (min-width:1200px) and (max-width:1599.98px){.ri-footer__info__logo{width:150px}}@media (max-width:767.98px){.ri-footer__info__logo{margin:0 auto -40px}}.ri-footer__info__map{padding:40px 0;max-width:600px}.ri-footer__info__map h4{font-size:24px;margin-bottom:20px}.ri-footer__info__map ul{list-style:none;padding-left:0;display:flex;flex-wrap:wrap;margin-bottom:-10px}.ri-footer__info__map ul li{width:25%;margin-bottom:10px}.ri-footer__info__map ul li a{color:#fff}.ri-footer__info__map ul li a:hover{color:#00b3b6}.ri-footer__info__map .ri-footer__info__map__group{flex-wrap:nowrap}.ri-footer__info__map .ri-footer__info__map__group li{width:auto}.ri-footer__info__map .ri-footer__info__map__group li:last-child:after{display:none}.ri-footer__info__map .ri-footer__info__map__group li:after{content:\"/\";margin:0 2px}@media (min-width:1200px) and (max-width:1599.98px){.ri-footer__info__map h4{font-size:22px;margin-bottom:20px}.ri-footer__info__map ul li{width:33.3%}}@media (min-width:768px) and (max-width:1199.98px){.ri-footer__info__map h4{font-size:22px;margin-bottom:20px}.ri-footer__info__map ul li{width:50%}}@media (max-width:767.98px){.ri-footer__info__map{display:none}}.ri-footer__info__contact{padding:40px 0;max-width:600px}.ri-footer__info__contact h4{font-size:24px;margin-bottom:20px}.ri-footer__info__contact ul{list-style:none;padding-left:0;display:flex;flex-wrap:wrap;margin-bottom:-10px}.ri-footer__info__contact ul li{margin-bottom:10px}.ri-footer__info__contact ul li a{color:#fff}.ri-footer__info__contact ul li a:hover{color:#00b3b6}.ri-footer__info__contact .ri-footer__info__contact__address,.ri-footer__info__contact .ri-footer__info__contact__email{flex-grow:1}@media (min-width:1200px) and (max-width:1599.98px){.ri-footer__info__contact h4{font-size:22px;margin-bottom:20px}.ri-footer__info__contact ul{display:block}}@media (max-width:1199.98px){.ri-footer__info__contact h4{font-size:22px;margin-bottom:20px}.ri-footer__info__contact ul{display:block}}@media (max-width:767.98px){.ri-footer__info__contact{padding:20px 0;text-align:center}}.ri-footer__info__media{padding:40px 0}.ri-footer__info__media ul{list-style:none;padding-left:0;display:flex;margin-bottom:0}.ri-footer__info__media ul li{padding:0 8px}@media (max-width:991.98px){.ri-footer__info__media ul{list-style:none;padding-left:0;display:flex;flex-wrap:wrap;margin-bottom:0}.ri-footer__info__media ul li{width:100%;text-align:right;padding:0 8px}}@media (max-width:767.98px){.ri-footer__info__media{padding:0 0 20px}.ri-footer__info__media ul{justify-content:center}.ri-footer__info__media ul li{width:auto}}.ri-footer__copyright{border-top:1px solid #fff;padding-top:20px;padding-bottom:40px;text-align:center;color:#fff}@media (max-width:767.98px){.ri-footer__copyright{padding-bottom:20px}}.ri-a:hover{color:#00b3b6}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=footer.js.map
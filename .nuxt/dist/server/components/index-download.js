exports.ids = [3];
exports.modules = {

/***/ 108:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(158);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("acf2390c", content, true, context)
};

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Download_vue_vue_type_style_index_0_id_6d688342_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(108);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Download_vue_vue_type_style_index_0_id_6d688342_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Download_vue_vue_type_style_index_0_id_6d688342_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Download_vue_vue_type_style_index_0_id_6d688342_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Download_vue_vue_type_style_index_0_id_6d688342_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-download[data-v-6d688342]{background-color:#f3f3f3}@media (max-width:1199.98px){.ri-download[data-v-6d688342]{padding-bottom:60px}}.ri-mainTitle[data-v-6d688342]{text-align:left;color:#395265;margin-bottom:0}.ri-mainTitle span[data-v-6d688342]{border-color:#395265}@media (max-width:767.98px){.ri-mainTitle[data-v-6d688342]{text-align:center;margin-bottom:40px}}.ri-download__group[data-v-6d688342]{display:flex;justify-content:space-between;align-items:center;color:#395265}.ri-download__group ul[data-v-6d688342]{display:flex;list-style:none;padding-left:0;margin-bottom:0}@media (max-width:767.98px){.ri-download__group[data-v-6d688342],.ri-download__group ul[data-v-6d688342]{display:block}}.ri-download__group__list[data-v-6d688342]{transform:scaleX(1);transform-origin:bottom right;margin-left:100px;padding-bottom:5px;border-bottom:1px solid #395265}.ri-download__group__list .ri-a[data-v-6d688342]{display:flex;justify-content:space-between}.ri-download__group__list .p[data-v-6d688342]{font-size:24px;display:flex;font-weight:300;margin-bottom:0;margin-right:40px}.ri-download__group__list .p .date[data-v-6d688342]{flex-shrink:0;font-style:italic;margin-right:20px}.ri-download__group__list[data-v-6d688342]:hover{color:#00b3b6;border-bottom:1px solid #00b3b6;transition:.5s;transform:scaleX(1.05);transform-origin:bottom right}.ri-download__group__list:hover .ri-more__link[data-v-6d688342]{color:#00b3b6}@media (min-width:992px) and (max-width:1199.98px){.ri-download__group__list[data-v-6d688342]{margin-left:60px}.ri-download__group__list .p[data-v-6d688342]{font-size:16px;margin-right:20px}}@media (min-width:768px) and (max-width:991.98px){.ri-download__group__list[data-v-6d688342]{margin-left:40px}.ri-download__group__list .p[data-v-6d688342]{font-size:16px;margin-right:10px}.ri-download__group__list .ri-more__link[data-v-6d688342]{display:none}}@media (max-width:767.98px){.ri-download__group__list[data-v-6d688342]{margin-left:0;margin-bottom:20px}.ri-download__group__list .p[data-v-6d688342]{font-size:14px;margin-right:10px}.ri-download__group__list[data-v-6d688342]:hover{transform:scaleX(1)}}.ri-more[data-v-6d688342]{display:inline-block;margin-top:0;margin-right:3px}.ri-more[data-v-6d688342],.ri-more__link[data-v-6d688342]{color:#395265}.ri-more__link[data-v-6d688342]:hover{color:#00b3b6;text-decoration:none}.ri-more__link[data-v-6d688342]:hover:after{transition:.4s;width:100px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/Download.vue?vue&type=template&id=6d688342&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-download ri-section"},[_vm._ssrNode("<div class=\"container\" data-v-6d688342>","</div>",[_vm._ssrNode("<div class=\"ri-download__group\" data-v-6d688342>","</div>",[_vm._ssrNode("<h2 class=\"ri-mainTitle\" data-v-6d688342><span data-v-6d688342>"+_vm._ssrEscape(_vm._s(_vm.$t('navigation.download')))+"</span></h2> "),_vm._ssrNode("<ul data-v-6d688342>","</ul>",[_vm._ssrNode("<li class=\"ri-download__group__list\" data-v-6d688342>","</li>",[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath('/downloadSystemMenu')}},[_c('span',{staticClass:"p"},[_vm._v("\n              "+_vm._s(_vm.$t('download.downloadSystemMenu.name'))+"\n            ")]),_vm._v(" "),_c('span',{staticClass:"ri-more"},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                "+_vm._s(_vm.$t('global.more'))+"\n              ")])])])],1),_vm._ssrNode(" "),_vm._ssrNode("<li class=\"ri-download__group__list\" data-v-6d688342>","</li>",[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath('/downloadTestData')}},[_c('span',{staticClass:"p"},[_vm._v("\n              "+_vm._s(_vm.$t('download.downloadTestData.name'))+"\n            ")]),_vm._v(" "),_c('span',{staticClass:"ri-more"},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                "+_vm._s(_vm.$t('global.more'))+"\n              ")])])])],1),_vm._ssrNode(" "),_vm._ssrNode("<li class=\"ri-download__group__list\" data-v-6d688342>","</li>",[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath('/downloadDetect')}},[_c('span',{staticClass:"p"},[_vm._v("\n              "+_vm._s(_vm.$t('download.downloadDetect.name'))+"\n            ")]),_vm._v(" "),_c('span',{staticClass:"ri-more"},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                "+_vm._s(_vm.$t('global.more'))+"\n              ")])])])],1)],2)],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/index/Download.vue?vue&type=template&id=6d688342&scoped=true&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/index/Download.vue

var script = {}
function injectStyles (context) {
  
  var style0 = __webpack_require__(157)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "6d688342",
  "686be0df"
  
)

/* harmony default export */ var Download = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index-download.js.map
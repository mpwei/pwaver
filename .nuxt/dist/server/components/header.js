exports.ids = [13];
exports.modules = {

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Header.vue?vue&type=template&id=05e28c18&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('b-navbar',{staticClass:"ri-header",attrs:{"toggleable":"xl","type":"light","variant":"light"}},[_c('div',{staticClass:"container"},[_c('b-navbar-brand',{staticClass:"ri-header__logo",attrs:{"to":_vm.localePath('/')}},[_c('h1',[_c('img',{staticClass:"mr-3",attrs:{"src":"/image/P-WAVER-Logo.svg","alt":"P-WAVER"}}),_vm._v("P-WAVER")])]),_vm._v(" "),_c('b-navbar-toggle',{staticClass:"ri-header__button",attrs:{"target":"nav-collapse"}},[_c('span',{staticClass:"icon-Menu-1"}),_vm._v(" "),_c('span',{staticClass:"icon-Menu"})]),_vm._v(" "),_c('b-collapse',{attrs:{"id":"nav-collapse","is-nav":""}},[_c('b-navbar-nav',{staticClass:"ml-auto py-4"},[_c('b-nav-item',{attrs:{"to":_vm.localePath('/')}},[_c('span',[_vm._v(_vm._s(_vm.$t('navigation.index')))])]),_vm._v(" "),_c('b-nav-item-dropdown',{attrs:{"text":_vm.$t('navigation.about')}},[_vm._l((_vm.$store.state.GlobalArticleData.aboutUS),function(content,index){return [_c('b-dropdown-item',{key:index,attrs:{"to":_vm.localePath(("/aboutUS#ri-" + (content.Slug)))}},[_c('span',[_vm._v(_vm._s(content.LanguageData[_vm.$i18n.locale].Name))])])]})],2),_vm._v(" "),_c('b-nav-item',{attrs:{"to":_vm.localePath('/news')}},[_c('span',[_vm._v(_vm._s(_vm.$t('navigation.news')))])]),_vm._v(" "),_c('b-nav-item-dropdown',{attrs:{"text":_vm.$t('navigation.product')}},[_vm._l((_vm.$store.state.GlobalArticleData.product),function(content,index){return [_c('b-dropdown-item',{key:index,attrs:{"to":_vm.localePath(("/" + (content.Slug)))}},[_c('span',[_vm._v(_vm._s(content.LanguageData[_vm.$i18n.locale].Name))])])]})],2),_vm._v(" "),_c('b-nav-item-dropdown',{attrs:{"text":_vm.$t('navigation.download')}},[_c('b-dropdown-item',{attrs:{"to":_vm.localePath('/downloadSystemMenu')}},[_c('span',[_vm._v(_vm._s(_vm.$t('download.downloadSystemMenu.name')))])]),_vm._v(" "),_c('b-dropdown-item',{attrs:{"to":_vm.localePath('/downloadTestData')}},[_c('span',[_vm._v(_vm._s(_vm.$t('download.downloadTestData.name')))])]),_vm._v(" "),_c('b-dropdown-item',{attrs:{"to":_vm.localePath('/downloadDetect')}},[_c('span',[_vm._v(_vm._s(_vm.$t('download.downloadDetect.name')))])])],1),_vm._v(" "),_c('b-nav-item',{attrs:{"to":_vm.localePath('/contact')}},[_c('span',[_vm._v(_vm._s(_vm.$t('navigation.contact')))])]),_vm._v(" "),_c('b-navbar-nav',{staticClass:"ri-header__language"},[_c('b-nav-item',{on:{"click":function($event){return _vm.$i18n.setLocale('zh-tw')}}},[_c('span',[_vm._v(_vm._s(_vm.$t('footer.language.zh')))])]),_vm._v(" "),_c('b-nav-item',{on:{"click":function($event){return _vm.$i18n.setLocale('en-us')}}},[_c('span',[_vm._v(_vm._s(_vm.$t('footer.language.en')))])])],1)],1)],1)],1)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/Header.vue?vue&type=template&id=05e28c18&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Header.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Headervue_type_script_lang_js_ = ({
  data() {
    return {};
  },

  mounted() {},

  methods: {}
});
// CONCATENATED MODULE: ./components/Header.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Headervue_type_script_lang_js_ = (Headervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/Header.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(81)
if (style0.__inject__) style0.__inject__(context)
var style1 = __webpack_require__(83)
if (style1.__inject__) style1.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_Headervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "05e28c18",
  "0f1deacc"
  
)

/* harmony default export */ var Header = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 75:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(82);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("47cadc0b", content, true, context)
};

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(84);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("2da68355", content, true, context)
};

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(75);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".navbar-light .navbar-nav .nav-link{color:#3d3d3d;font-weight:300;padding-left:20px;padding-right:20px;padding-bottom:0}.navbar-light .navbar-nav .nav-link:focus,.navbar-light .navbar-nav .nav-link:hover{color:#36679c}.navbar-light .navbar-nav .nav-link:focus span,.navbar-light .navbar-nav .nav-link:hover span{position:relative;display:inline-block}.navbar-light .navbar-nav .nav-link:focus span:before,.navbar-light .navbar-nav .nav-link:hover span:before{content:\"\";width:100%;position:absolute;border-bottom:2px solid #36679c;left:50%;transform:translate(-50%);bottom:-2px}@media (max-width:1199.98px){.navbar-light .navbar-nav{text-align:center}.navbar-light .navbar-nav .nav-link{margin:5px 0}}@media (max-width:767.98px){.navbar-light .navbar-nav .nav-link{margin:2px 0}}.navbar-collapse{min-height:0!important;transition:.5s}.navbar-collapse.show{min-height:100vh!important}.dropdown-menu{text-align:center;left:50%;transform:translate(-50%);font-size:22px;border:none;box-shadow:0 3px 6px rgba(0,0,0,.2);padding-bottom:20px;margin-top:25px}.dropdown-menu .dropdown-item{font-weight:300;position:relative}.dropdown-menu .dropdown-item:hover{color:#36679c;background:transparent}.dropdown-menu .dropdown-item:hover span{position:relative;display:inline-block}.dropdown-menu .dropdown-item:hover span:before{content:\"\";width:100%;position:absolute;border-bottom:2px solid #36679c;left:50%;transform:translate(-50%);bottom:-2px}@media (min-width:1440px) and (max-width:1599.98px){.dropdown-menu{font-size:20px}}@media (min-width:1200px) and (max-width:1439.98px){.dropdown-menu{font-size:16px;margin-top:18px}}@media (max-width:1199.98px){.dropdown-menu{left:0;font-size:20px;transform:translate(0);box-shadow:none;background:rgba(54,103,156,.2);margin-top:5px}}@media (max-width:767.98px){.dropdown-menu{font-size:16px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(76);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-header[data-v-05e28c18]{position:fixed;top:0;width:100%;padding:1px 1rem;font-size:26px;box-shadow:0 3px 6px rgba(0,0,0,.2);z-index:99}@media (min-width:1440px) and (max-width:1599.98px){.ri-header[data-v-05e28c18]{font-size:22px}}@media (min-width:1200px) and (max-width:1439.98px){.ri-header[data-v-05e28c18]{font-size:18px}}@media (min-width:768px) and (max-width:1200px){.ri-header[data-v-05e28c18]{font-size:22px}}@media (max-width:767.98px){.ri-header[data-v-05e28c18]{font-size:18px}}.ri-header__button[data-v-05e28c18]{color:#3d3d3d;border:none}.ri-header__button[data-v-05e28c18]:hover{color:#36679c}.ri-header__button .icon-Menu[data-v-05e28c18],.ri-header__button[aria-expanded=true] .icon-Menu-1[data-v-05e28c18]{display:none}.ri-header__button[aria-expanded=true] .icon-Menu[data-v-05e28c18]{display:block}.ri-header__logo[data-v-05e28c18]{padding:0;font-size:0;width:172px;white-space:nowrap;overflow:hidden}.ri-header__logo h1[data-v-05e28c18]{margin-bottom:0}.ri-header__logo img[data-v-05e28c18]{width:100%}@media (max-width:1439.98px){.ri-header__logo[data-v-05e28c18]{width:120px}}@media (max-width:991.98px){.ri-header__logo[data-v-05e28c18]{width:100px}}.ri-header__language[data-v-05e28c18]{display:flex}.ri-header__language .nav-item:last-child .nav-link[data-v-05e28c18]:after{display:none}.ri-header__language .nav-link[data-v-05e28c18]{position:relative;padding-left:10px;padding-right:10px}.ri-header__language .nav-link[data-v-05e28c18]:hover:before{width:calc(100% - 20px)}.ri-header__language .nav-link[data-v-05e28c18]:after{content:\"/\";position:absolute;right:-4px}@media (max-width:1200px){.ri-header__language[data-v-05e28c18]{flex-direction:row;justify-content:center}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=header.js.map
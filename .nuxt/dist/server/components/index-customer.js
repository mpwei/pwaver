exports.ids = [2];
exports.modules = {

/***/ 107:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(156);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("987d94a2", content, true, context)
};

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_style_index_0_id_3b9fd63e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(107);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_style_index_0_id_3b9fd63e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_style_index_0_id_3b9fd63e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_style_index_0_id_3b9fd63e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_style_index_0_id_3b9fd63e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-customer[data-v-3b9fd63e]{background-color:#fff}.ri-customer ul[data-v-3b9fd63e]{display:flex;list-style:none;padding-left:0;flex-wrap:wrap;margin-bottom:0}@media (max-width:767.98px){.ri-customer ul[data-v-3b9fd63e]{flex-wrap:wrap}}.ri-customer__group__img[data-v-3b9fd63e]{position:relative;padding-bottom:75%;height:0;overflow:hidden}.ri-customer__group__img img[data-v-3b9fd63e]{position:absolute;left:0;top:0;width:100%;height:100%;-o-object-fit:cover;object-fit:cover}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/Customer.vue?vue&type=template&id=3b9fd63e&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-customer ri-section"},[_vm._ssrNode("<div class=\"container\" data-v-3b9fd63e>","</div>",[_vm._ssrNode("<h2 class=\"ri-mainTitle\" data-v-3b9fd63e><span data-v-3b9fd63e>"+_vm._ssrEscape(_vm._s(_vm.$t('index.customerTitle')))+"</span></h2> "),_c('client-only',[_c('ul',{staticClass:"ri-customer__group row",attrs:{"data-aos":"zoom-out"}},_vm._l((_vm.carouselBottom),function(carousel){return _c('li',{key:carousel.URL,staticClass:"col-4 col-md-3 col-xl-2"},[_c('div',{staticClass:"ri-customer__group__img"},[_c('img',{attrs:{"src":carousel.URL,"alt":carousel.Alt,"title":carousel.Title}})])])}),0),_vm._v(" "),_c('template',{slot:"placeholder"},[_c('ul',{staticClass:"ri-customer__group row"},_vm._l((_vm.carouselBottom),function(carousel,index){return _c('li',{key:index,staticClass:"col-4 col-md-3 col-xl-2"},[_c('div',{staticClass:"ri-customer__group__img"},[_c('img',{attrs:{"src":carousel.URL,"alt":carousel.Alt,"title":carousel.Title}})])])}),0)])],2)],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/index/Customer.vue?vue&type=template&id=3b9fd63e&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/Customer.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Customervue_type_script_lang_js_ = ({
  props: {
    carouselBottom: {
      type: Array,
      default: () => []
    }
  }
});
// CONCATENATED MODULE: ./components/index/Customer.vue?vue&type=script&lang=js&
 /* harmony default export */ var index_Customervue_type_script_lang_js_ = (Customervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/index/Customer.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(155)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  index_Customervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "3b9fd63e",
  "5bfafd35"
  
)

/* harmony default export */ var Customer = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index-customer.js.map
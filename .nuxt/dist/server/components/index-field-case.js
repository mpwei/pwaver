exports.ids = [4];
exports.modules = {

/***/ 109:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(160);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("5ff3b74d", content, true, context)
};

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldCase_vue_vue_type_style_index_0_id_de609462_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(109);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldCase_vue_vue_type_style_index_0_id_de609462_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldCase_vue_vue_type_style_index_0_id_de609462_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldCase_vue_vue_type_style_index_0_id_de609462_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldCase_vue_vue_type_style_index_0_id_de609462_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 160:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-field[data-v-de609462]{background-color:#f3f3f3}.ri-field ul[data-v-de609462]{display:flex;list-style:none;padding-left:0;flex-wrap:wrap}@media (max-width:767.98px){.ri-field ul[data-v-de609462]{flex-wrap:wrap}}.ri-field-description p[data-v-de609462]{font-size:30px;margin-bottom:24px}.ri-field-description ul[data-v-de609462]{list-style-type:disc;padding-left:20px;margin-bottom:40px;justify-content:center}.ri-field-description ul li[data-v-de609462]{font-size:24px;color:#00b3b6;margin-right:50px}.ri-field-description ul li[data-v-de609462]:last-child{margin-right:0}.ri-field-description ul li span[data-v-de609462]{color:#3d3d3d}@media (max-width:1199.98px){.ri-field-description ul li[data-v-de609462]{font-size:16px}}@media (max-width:767.98px){.ri-field-description p[data-v-de609462]{font-size:18px}.ri-field-description ul[data-v-de609462]{display:block;text-align:left}.ri-field-description ul li[data-v-de609462]{font-size:14px}}.ri-field__card[data-v-de609462]{width:calc(33% - 53px);border:1px solid #707070;background-color:#fff;box-shadow:0 3px 6px rgba(0,0,0,.2);position:relative;margin-bottom:60px;margin-right:40px;margin-left:40px;transition:.5s}.ri-field__card[data-v-de609462]:hover{transition:.5s;border:1px solid #00b3b6}.ri-field__card:hover .ri-field__card__text h3[data-v-de609462]{transition:.5s;color:#00b3b6}.ri-field__card:hover .ri-search[data-v-de609462]{transition:.5s;opacity:1}.ri-field__card[data-v-de609462]:nth-child(3n+1){margin-left:0}.ri-field__card[data-v-de609462]:nth-child(3n){margin-right:0}@media (max-width:991.98px){.ri-field__card[data-v-de609462]{width:calc(33% - 25px);margin-right:20px;margin-left:20px}.ri-field__card[data-v-de609462]:after{right:-20px}}@media (max-width:767.98px){.ri-field__card[data-v-de609462]{width:calc(50% - 10px);margin-right:10px;margin-left:10px;margin-bottom:40px}.ri-field__card[data-v-de609462]:nth-child(odd){margin-left:0}.ri-field__card[data-v-de609462]:nth-child(3n){margin-right:auto}.ri-field__card[data-v-de609462]:nth-child(2n){margin-right:0}}.ri-field__card__img[data-v-de609462]{position:relative;padding-bottom:75%;height:0;overflow:hidden}.ri-field__card__img img[data-v-de609462]{position:absolute;left:0;top:0;width:100%;height:100%;-o-object-fit:cover;object-fit:cover}.ri-field-videoBox[data-v-de609462]{max-width:1000px;margin:0 auto}.ri-field-videoBox div[data-v-de609462]{position:relative;width:100%;height:0;padding-bottom:56.25%}.ri-field-videoBox iframe[data-v-de609462]{position:absolute;top:0;left:0;width:100%;height:100%}.ri-search[data-v-de609462]{transition:.5s;opacity:0;position:absolute;z-index:2;left:50%;top:50%;transform:translate(-50%,-35%)}.ri-search .ri-search__bg[data-v-de609462]{width:80px;height:80px;background-color:#00b3b6;opacity:.5;border-radius:50%}.ri-search .ri-search__search[data-v-de609462]{font-size:30px;color:#fff;position:absolute;z-index:3;top:50%;left:50%;transform:translate(-50%,-50%);transition:.5s}@media (max-width:1199.98px){.ri-search .ri-search__bg[data-v-de609462]{width:60px;height:60px}.ri-search .ri-search__search[data-v-de609462]{font-size:22px}}@media (max-width:767.98px){.ri-search .ri-search__bg[data-v-de609462]{width:40px;height:40px}.ri-search .ri-search__search[data-v-de609462]{font-size:15px}}.ri-field__card__text[data-v-de609462]{padding:20px 40px;text-align:center}.ri-field__card__text h3[data-v-de609462]{transition:.5s;font-size:26px;margin-bottom:0}@media (min-width:992px) and (max-width:1439.98px){.ri-field__card__text h3[data-v-de609462]{font-size:24px}}@media (max-width:991.98px){.ri-field__card__text[data-v-de609462]{padding:20px}.ri-field__card__text h3[data-v-de609462]{font-size:20px}}@media (max-width:767.98px){.ri-field__card__text[data-v-de609462]{padding:10px}.ri-field__card__text h3[data-v-de609462]{font-size:16px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 184:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/FieldCase.vue?vue&type=template&id=de609462&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-field ri-section"},[_vm._ssrNode("<div class=\"container\" data-v-de609462>","</div>",[_vm._ssrNode("<h2 class=\"ri-mainTitle\" data-v-de609462><span data-v-de609462>"+_vm._ssrEscape(_vm._s(_vm.$t('index.fieldCaseTitle')))+"</span></h2> <div class=\"ri-field-description\" data-v-de609462><p class=\"text-center\" data-v-de609462>\n        P-Waver Already Provided Earthquake Early Warning Systems For\n      </p> <ul data-v-de609462><li data-v-de609462><span data-v-de609462>High-Tech Plant : 7</span></li> <li data-v-de609462><span data-v-de609462>Science Park : 2</span></li> <li data-v-de609462><span data-v-de609462>Taiwan High Speed Rail : 1</span></li> <li data-v-de609462><span data-v-de609462>Campus : 3500</span></li> <li data-v-de609462><span data-v-de609462>Hospital : 2</span></li></ul></div> "),_c('client-only',[_c('ul',_vm._l((_vm.$store.state.GlobalArticleData.fieldCase),function(data,index){return _c('li',{key:index,staticClass:"ri-field__card",attrs:{"data-aos-delay":(index%3)*200,"data-aos":"fade-up-right"}},[_c('a',{staticClass:"js-fancybox ri-about__video__facy ri-a",attrs:{"data-caption":(data.LanguageData[_vm.$i18n.locale].Name || data.Name),"data-fancybox":"gallery","href":(data.LanguageData[_vm.$i18n.locale].Cover || data.Cover)}},[_c('div',{staticClass:"ri-field__card__img"},[_c('img',{attrs:{"src":(data.LanguageData[_vm.$i18n.locale].Cover || data.Cover),"alt":(data.LanguageData[_vm.$i18n.locale].Name || data.Name)}}),_vm._v(" "),_c('div',{staticClass:"ri-search"},[_c('div',{staticClass:"ri-search__bg"}),_vm._v(" "),_c('b-icon',{staticClass:"ri-search__search",attrs:{"icon":"search","aria-hidden":"true"}})],1)]),_vm._v(" "),_c('div',{staticClass:"ri-field__card__text"},[_c('h3',[_vm._v("\n                "+_vm._s((data.LanguageData[_vm.$i18n.locale].Name || data.Name))+"\n              ")])])])])}),0),_vm._v(" "),_c('template',{slot:"placeholder"},[_c('ul',_vm._l((_vm.$store.state.GlobalArticleData.fieldCase),function(data,index){return _c('li',{key:index,staticClass:"ri-field__card",attrs:{"data-aos-delay":(index%3)*200,"data-aos":"fade-up-right"}},[_c('a',{staticClass:"js-fancybox ri-about__video__facy ri-a",attrs:{"data-caption":(data.LanguageData[_vm.$i18n.locale].Name || data.Name),"data-fancybox":"gallery","href":(data.LanguageData[_vm.$i18n.locale].Cover || data.Cover)}},[_c('div',{staticClass:"ri-field__card__img"},[_c('img',{attrs:{"src":(data.LanguageData[_vm.$i18n.locale].Cover || data.Cover),"alt":(data.LanguageData[_vm.$i18n.locale].Name || data.Name)}}),_vm._v(" "),_c('div',{staticClass:"ri-search"},[_c('div',{staticClass:"ri-search__bg"}),_vm._v(" "),_c('b-icon',{staticClass:"ri-search__search",attrs:{"icon":"search","aria-hidden":"true"}})],1)]),_vm._v(" "),_c('div',{staticClass:"ri-field__card__text"},[_c('h3',[_vm._v("\n                  "+_vm._s((data.LanguageData[_vm.$i18n.locale].Name || data.Name))+"\n                ")])])])])}),0)])],2),_vm._ssrNode(" <div class=\"ri-field-videoBox\" data-v-de609462><div data-v-de609462><iframe src=\"https://www.youtube.com/embed/0UTSXGSY4X0\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen=\"allowfullscreen\" data-v-de609462></iframe></div></div>")],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/index/FieldCase.vue?vue&type=template&id=de609462&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/FieldCase.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var FieldCasevue_type_script_lang_js_ = ({
  props: {
    fieldCaseTitle: {
      type: Object,
      default: () => ({})
    },
    allFieldCase: {
      type: Array,
      default: () => []
    }
  },

  mounted() {
    $('.js-fancybox').fancybox({
      infobar: false,
      buttons: ['close']
    });
  }

});
// CONCATENATED MODULE: ./components/index/FieldCase.vue?vue&type=script&lang=js&
 /* harmony default export */ var index_FieldCasevue_type_script_lang_js_ = (FieldCasevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/index/FieldCase.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(159)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  index_FieldCasevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "de609462",
  "da2661fa"
  
)

/* harmony default export */ var FieldCase = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index-field-case.js.map
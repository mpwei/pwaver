exports.ids = [14];
exports.modules = {

/***/ 112:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(166);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("cc71d2de", content, true, context)
};

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDSpace_vue_vue_type_style_index_0_id_fa09dff6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(112);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDSpace_vue_vue_type_style_index_0_id_fa09dff6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDSpace_vue_vue_type_style_index_0_id_fa09dff6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDSpace_vue_vue_type_style_index_0_id_fa09dff6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDSpace_vue_vue_type_style_index_0_id_fa09dff6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-threeD[data-v-fa09dff6]{background-color:#f3f3f3}.ri-threeD ul[data-v-fa09dff6]{display:flex;list-style:none;padding-left:0}@media (max-width:767.98px){.ri-threeD ul[data-v-fa09dff6]{flex-wrap:wrap}}.ri-threeD__card[data-v-fa09dff6]{flex:1;position:relative;margin-right:40px;margin-left:40px}.ri-threeD__card:hover p[data-v-fa09dff6]{color:#3d3d3d}.ri-threeD__card:hover .ri-subTitle[data-v-fa09dff6]{transition:.5s;color:#00b3b6}.ri-threeD__card:hover .ri-more__link[data-v-fa09dff6]{color:#00b3b6;text-decoration:none}.ri-threeD__card:hover .ri-more__link[data-v-fa09dff6]:after{transition:.5s;width:90px}.ri-threeD__card[data-v-fa09dff6]:first-child{margin-left:0}.ri-threeD__card[data-v-fa09dff6]:last-child{margin-right:0}.ri-threeD__card[data-v-fa09dff6]:last-child:after{display:none}.ri-threeD__card[data-v-fa09dff6]:after{content:\"\";position:absolute;top:0;right:-40px;border-left:1px solid #d6d6d6;height:100%}@media (max-width:991.98px){.ri-threeD__card[data-v-fa09dff6]{margin-right:20px;margin-left:20px}.ri-threeD__card[data-v-fa09dff6]:after{right:-20px}}@media (max-width:767.98px){.ri-threeD__card[data-v-fa09dff6]{flex:auto;width:100%;margin-right:0;margin-left:0;margin-bottom:40px}.ri-threeD__card[data-v-fa09dff6]:last-child{margin-bottom:0}.ri-threeD__card[data-v-fa09dff6]:after{display:none}}.ri-threeD__card__img[data-v-fa09dff6]{position:relative;margin-bottom:40px;padding-bottom:75%;height:0;overflow:hidden}.ri-threeD__card__img img[data-v-fa09dff6]{position:absolute;left:0;top:0;width:100%;height:100%;-o-object-fit:cover;object-fit:cover}@media (max-width:767.98px){.ri-threeD__card__img[data-v-fa09dff6]{margin-bottom:20px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/ThreeDSpace.vue?vue&type=template&id=fa09dff6&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-threeD ri-section"},[_vm._ssrNode("<div class=\"container\" data-v-fa09dff6>","</div>",[_vm._ssrNode("<h2 class=\"ri-mainTitle\" data-v-fa09dff6><span data-v-fa09dff6>"+_vm._ssrEscape(_vm._s(_vm.$t('navigation.3DSpace')))+"</span></h2> "),_c('client-only',[_c('ul',{attrs:{"data-aos":"zoom-in"}},_vm._l((_vm.$store.state.GlobalArticleData.threeD),function(threeDItem,index){return _c('li',{key:index,staticClass:"ri-threeD__card"},[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath({name: ("" + (threeDItem.Slug))})}},[_c('div',{staticClass:"ri-threeD__card__img"},[_c('img',{attrs:{"src":(threeDItem.LanguageData[_vm.$i18n.locale].Cover || threeDItem.Cover),"alt":(threeDItem.LanguageData[_vm.$i18n.locale].Name || threeDItem.Name)}})]),_vm._v(" "),_c('div',{staticClass:"ri-threeD__card__text"},[_c('h3',{staticClass:"ri-subTitle"},[_vm._v("\n                "+_vm._s(threeDItem.LanguageData[_vm.$i18n.locale].Name || threeDItem.Name)+"\n              ")]),_vm._v(" "),_c('p',[_vm._v("\n                "+_vm._s(threeDItem.LanguageData[_vm.$i18n.locale].Introduction || threeDItem.Introduction)+"\n              ")]),_vm._v(" "),_c('a',{staticClass:"ri-more",attrs:{"href":"javascript:;"}},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                  "+_vm._s(_vm.$t('global.more'))+"\n                ")])])])])],1)}),0)]),_vm._ssrNode(" "),_vm._ssrNode("<template data-v-fa09dff6>","</template>",[_vm._ssrNode("<ul class=\"d-none\" data-v-fa09dff6>","</ul>",_vm._l((_vm.$store.state.GlobalArticleData.threeD),function(threeDItem,index){return _vm._ssrNode("<li class=\"ri-threeD__card\" data-v-fa09dff6>","</li>",[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath({name: ("" + (threeDItem.Slug))})}},[_c('div',{staticClass:"ri-threeD__card__img"},[_c('img',{attrs:{"src":(threeDItem.LanguageData[_vm.$i18n.locale].Cover || threeDItem.Cover),"alt":(threeDItem.LanguageData[_vm.$i18n.locale].Name || threeDItem.Name)}})]),_vm._v(" "),_c('div',{staticClass:"ri-threeD__card__text"},[_c('h3',{staticClass:"ri-subTitle"},[_vm._v("\n                "+_vm._s(threeDItem.LanguageData[_vm.$i18n.locale].Name || threeDItem.Name)+"\n              ")]),_vm._v(" "),_c('p',[_vm._v("\n                "+_vm._s(threeDItem.LanguageData[_vm.$i18n.locale].Introduction || threeDItem.Introduction)+"\n              ")]),_vm._v(" "),_c('a',{staticClass:"ri-more",attrs:{"href":"javascript:;"}},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                  "+_vm._s(_vm.$t('global.more'))+"\n                ")])])])])],1)}),0)])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/index/ThreeDSpace.vue?vue&type=template&id=fa09dff6&scoped=true&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/index/ThreeDSpace.vue

var script = {}
function injectStyles (context) {
  
  var style0 = __webpack_require__(165)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "fa09dff6",
  "3577deb9"
  
)

/* harmony default export */ var ThreeDSpace = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index-three-dspace.js.map
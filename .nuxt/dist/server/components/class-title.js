exports.ids = [11];
exports.modules = {

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=template&id=458214ec&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container ri-container"},[_vm._ssrNode("<div class=\"d-none d-md-block ri-breadCrumb\">","</div>",[(_vm.classTitle.Slug === 'threeD')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ThreeDSpace')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'product')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ProductAndServices')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'download')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#Download')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):_c('nuxt-link',{attrs:{"to":_vm.localePath(("/" + (_vm.classTitle.Slug)))}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]),_vm._ssrNode(" "),_c('b-icon',{attrs:{"icon":"chevron-right","aria-hidden":"true"}}),_vm._ssrNode(" "),_c('nuxt-link',{attrs:{"to":""}},[_vm._v("\n      "+_vm._s(_vm.singleData.Name)+"\n    ")])],2),_vm._ssrNode(" <div class=\"d-block d-md-none ri-breadCrumb\"><h2 class=\"ri-mainTitle\"><span>"+_vm._ssrEscape(_vm._s(_vm.classTitle.Name))+"</span></h2></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=template&id=458214ec&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ClassTitlevue_type_script_lang_js_ = ({
  props: {
    classTitle: {
      type: Object,
      default: () => ({})
    },
    languageTitle: {
      type: Object,
      default: () => ({})
    },
    singleData: {
      type: Object,
      default: null
    }
  }
});
// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ClassTitlevue_type_script_lang_js_ = (ClassTitlevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/ClassTitle.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ClassTitlevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "36d007cd"
  
)

/* harmony default export */ var ClassTitle = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=class-title.js.map
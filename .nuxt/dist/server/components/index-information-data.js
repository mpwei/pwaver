exports.ids = [5];
exports.modules = {

/***/ 110:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(162);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("721e54b0", content, true, context)
};

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InformationData_vue_vue_type_style_index_0_id_eff9da48_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(110);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InformationData_vue_vue_type_style_index_0_id_eff9da48_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InformationData_vue_vue_type_style_index_0_id_eff9da48_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InformationData_vue_vue_type_style_index_0_id_eff9da48_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InformationData_vue_vue_type_style_index_0_id_eff9da48_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-InformationData[data-v-eff9da48]{display:flex;justify-content:center;align-items:center;padding:100px 0}.ri-InformationData ul[data-v-eff9da48]{padding-left:0;display:flex;list-style:none;margin-bottom:0}.ri-InformationData ul li[data-v-eff9da48]{position:relative;padding:0 80px;display:flex;flex-direction:column;align-items:center}.ri-InformationData ul li[data-v-eff9da48]:after{position:absolute;content:\"\";display:block;height:100%;right:0;top:0;border-left:1px solid #d6d6d6}.ri-InformationData ul li[data-v-eff9da48]:last-child:after{display:none}.ri-InformationData ul h4[data-v-eff9da48]{font-size:60px}.ri-InformationData ul p[data-v-eff9da48]{margin-bottom:0;font-size:24px}@media (min-width:1200px) and (max-width:1439.98px){.ri-InformationData[data-v-eff9da48]{padding:80px 0}.ri-InformationData ul li[data-v-eff9da48]{padding:0 60px}.ri-InformationData ul h4[data-v-eff9da48]{font-size:40px}}@media (min-width:992px) and (max-width:1199.98px){.ri-InformationData[data-v-eff9da48]{padding:60px 0}.ri-InformationData ul li[data-v-eff9da48]{padding:0 40px}.ri-InformationData ul h4[data-v-eff9da48]{font-size:30px}.ri-InformationData ul p[data-v-eff9da48]{font-size:16px}}@media (min-width:768px) and (max-width:991.98px){.ri-InformationData[data-v-eff9da48]{padding:40px 0}.ri-InformationData ul li[data-v-eff9da48]{padding:0 20px}.ri-InformationData ul h4[data-v-eff9da48]{font-size:30px}.ri-InformationData ul p[data-v-eff9da48]{font-size:16px}}@media (max-width:767.98px){.ri-InformationData[data-v-eff9da48]{padding:40px 16px}.ri-InformationData ul[data-v-eff9da48]{flex-wrap:wrap}.ri-InformationData ul li[data-v-eff9da48]{width:50%;padding:10px 20px}.ri-InformationData ul li[data-v-eff9da48]:nth-child(2):after{display:none}.ri-InformationData ul h4[data-v-eff9da48]{font-size:20px}.ri-InformationData ul p[data-v-eff9da48]{font-size:14px;text-align:center}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/InformationData.vue?vue&type=template&id=eff9da48&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-InformationData container"},[_vm._ssrNode("<ul data-v-eff9da48>","</ul>",[_vm._ssrNode("<li data-aos=\"fade-up\" data-v-eff9da48>","</li>",[_vm._ssrNode("<h4 data-v-eff9da48>","</h4>",[_c('client-only',[_c('number',{ref:"number1",attrs:{"from":0,"to":2020,"duration":5}}),_vm._v(" "),_c('template',{slot:"placeholder"},[_c('p',{staticClass:"d-none"},[_vm._v("\n              2020\n            ")])])],2)],1),_vm._ssrNode(" <p data-v-eff9da48>Year Established</p>")],2),_vm._ssrNode(" "),_vm._ssrNode("<li data-aos=\"fade-up\" data-aos-delay=\"200\" data-v-eff9da48>","</li>",[_vm._ssrNode("<h4 data-v-eff9da48>","</h4>",[_c('client-only',[_c('number',{ref:"number2",attrs:{"from":0,"to":3000,"duration":5}}),_vm._v("+\n          "),_c('template',{slot:"placeholder"},[_c('p',{staticClass:"d-none"},[_vm._v("\n              3000 +\n            ")])])],2)],1),_vm._ssrNode(" <p data-v-eff9da48>Site Completed</p>")],2),_vm._ssrNode(" "),_vm._ssrNode("<li data-aos=\"fade-up\" data-aos-delay=\"400\" data-v-eff9da48>","</li>",[_vm._ssrNode("<h4 data-v-eff9da48>","</h4>",[_c('client-only',[_c('number',{ref:"number3",attrs:{"from":0,"to":9,"duration":5}}),_vm._v("+\n          "),_c('template',{slot:"placeholder"},[_c('p',{staticClass:"d-none"},[_vm._v("\n              9 +\n            ")])])],2)],1),_vm._ssrNode(" <p data-v-eff9da48>High Tech Industry</p>")],2),_vm._ssrNode(" "),_vm._ssrNode("<li data-aos=\"fade-up\" data-aos-delay=\"600\" data-v-eff9da48>","</li>",[_vm._ssrNode("<h4 data-v-eff9da48>","</h4>",[_c('client-only',[_c('number',{ref:"number4",attrs:{"from":0,"to":3,"duration":5}}),_vm._v("+\n          "),_c('template',{slot:"placeholder"},[_c('p',{staticClass:"d-none"},[_vm._v("\n              3 +\n            ")])])],2)],1),_vm._ssrNode(" <p data-v-eff9da48>Overseas Service</p>")],2)],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/index/InformationData.vue?vue&type=template&id=eff9da48&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/InformationData.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var InformationDatavue_type_script_lang_js_ = ({
  props: {
    allThreeD: {
      type: Array,
      default: () => []
    }
  },

  data() {
    return {};
  },

  mounted() {
    window.addEventListener('scroll', this.handleScroll);
  },

  methods: {
    handleScroll() {
      const t = document.documentElement.scrollTop || document.body.scrollTop;

      if (t === 300) {
        this.playAnimation();
      }
    },

    playAnimation() {
      this.$refs.number1.play();
      this.$refs.number2.play();
      this.$refs.number3.play();
      this.$refs.number4.play();
    }

  }
});
// CONCATENATED MODULE: ./components/index/InformationData.vue?vue&type=script&lang=js&
 /* harmony default export */ var index_InformationDatavue_type_script_lang_js_ = (InformationDatavue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/index/InformationData.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(161)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  index_InformationDatavue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "eff9da48",
  "7d971bef"
  
)

/* harmony default export */ var InformationData = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index-information-data.js.map
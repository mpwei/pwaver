exports.ids = [1];
exports.modules = {

/***/ 106:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(154);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("dae29948", content, true, context)
};

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Banner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(106);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Banner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Banner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Banner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Banner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 154:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-carousel{position:relative}.ri-carousel .carousel-item{height:calc(100vh - 99px)}.ri-carousel .carousel-item img{height:100%;-o-object-fit:cover;object-fit:cover;-o-object-position:center center;object-position:center center}@media (min-width:992px) and (max-width:1439.98px){.ri-carousel .carousel-item{height:calc(100vh - 69px)}}@media (min-width:768px) and (max-width:991.98px){.ri-carousel .carousel-item{height:calc(100vh - 66px);max-height:calc(100vh - 223px)}}@media (max-width:767.98px){.ri-carousel .carousel-item{height:800px;max-height:calc(100vh - 304px)}}.ri-carousel .carousel-caption{position:absolute;z-index:98;width:calc(100% - 60px);height:100%;left:50%;right:auto;transform:translate(-50%);display:flex;flex-direction:column;justify-content:center;color:#fff;text-align:left}.ri-carousel .carousel-caption>div{margin-left:100px;margin-right:100px}.ri-carousel .carousel-caption>div h2{font-size:40px;margin-bottom:24px}.ri-carousel .carousel-caption>div p{color:#dbdbdb;font-size:32px;margin-bottom:56px}@media (min-width:1440px) and (max-width:1599.98px){.ri-carousel .carousel-caption>div{margin-right:80px;margin-left:80px}.ri-carousel .carousel-caption>div h2{font-size:80px;margin-bottom:20px}.ri-carousel .carousel-caption>div p{font-size:24px;margin-bottom:52px}}@media (min-width:1200px) and (max-width:1439.98px){.ri-carousel .carousel-caption>div{margin-right:60px;margin-left:60px}.ri-carousel .carousel-caption>div h2{font-size:60px}.ri-carousel .carousel-caption>div p{font-size:20px;margin-bottom:40px}}@media (min-width:992px) and (max-width:1199.98px){.ri-carousel .carousel-caption>div{margin-right:40px;margin-left:40px}.ri-carousel .carousel-caption>div h2{font-size:50px}.ri-carousel .carousel-caption>div p{font-size:20px;margin-bottom:40px}}@media (min-width:768px) and (max-width:991.98px){.ri-carousel .carousel-caption>div{margin-right:40px;margin-left:40px}.ri-carousel .carousel-caption>div h2{font-size:40px}.ri-carousel .carousel-caption>div p{font-size:18px;margin-bottom:40px}}@media (max-width:767.98px){.ri-carousel .carousel-caption{text-align:center}.ri-carousel .carousel-caption>div{margin:0 auto}.ri-carousel .carousel-caption>div h2{font-size:28px;margin-bottom:16px}.ri-carousel .carousel-caption>div p{font-size:16px;margin-bottom:40px}}.ri-carousel .carousel-indicators li{width:15px;height:15px;border-radius:50%}.ri-carousel .carousel-indicators li.active{background-color:#00b3b6}@media (max-width:767.98px){.ri-carousel .carousel-indicators li{width:10px;height:10px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/Banner.vue?vue&type=template&id=f70c8d54&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-carousel"},[_c('b-carousel',{attrs:{"id":"carousel-1","fade":"","indicators":"","background":"#e6e9ed"},model:{value:(_vm.slide),callback:function ($$v) {_vm.slide=$$v},expression:"slide"}},_vm._l((_vm.carousel),function(carouselItem,index){return _c('b-carousel-slide',{key:carouselItem.Title+index,attrs:{"img-src":carouselItem.URL,"img-alt":carouselItem.Alt}},[_c('div',{},[_c('h2',{domProps:{"innerHTML":_vm._s(carouselItem.LanguageData[_vm.$i18n.locale].Title)}}),_vm._v(" "),_c('p',[_vm._v(_vm._s(carouselItem.LanguageData[_vm.$i18n.locale].Content))]),_vm._v(" "),_c('a',{directives:[{name:"show",rawName:"v-show",value:(carouselItem.LanguageData[_vm.$i18n.locale].Link),expression:"carouselItem.LanguageData[$i18n.locale].Link"}],staticClass:"ri-button ri-button--white",attrs:{"href":carouselItem.LanguageData[_vm.$i18n.locale].Link}},[_vm._v("\n            "+_vm._s(_vm.$t('global.viewDetail'))+"\n          ")])])])}),1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/index/Banner.vue?vue&type=template&id=f70c8d54&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/Banner.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Bannervue_type_script_lang_js_ = ({
  props: {
    carousel: {
      type: Array,
      default: () => []
    }
  },

  data() {
    return {
      slide: 0,
      sliding: null
    };
  }

});
// CONCATENATED MODULE: ./components/index/Banner.vue?vue&type=script&lang=js&
 /* harmony default export */ var index_Bannervue_type_script_lang_js_ = (Bannervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/index/Banner.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(153)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  index_Bannervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "2b700943"
  
)

/* harmony default export */ var Banner = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index-banner.js.map
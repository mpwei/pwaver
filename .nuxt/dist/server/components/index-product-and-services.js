exports.ids = [6];
exports.modules = {

/***/ 111:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(164);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("4d1b4d92", content, true, context)
};

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAndServices_vue_vue_type_style_index_0_id_a17563fc_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(111);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAndServices_vue_vue_type_style_index_0_id_a17563fc_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAndServices_vue_vue_type_style_index_0_id_a17563fc_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAndServices_vue_vue_type_style_index_0_id_a17563fc_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAndServices_vue_vue_type_style_index_0_id_a17563fc_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-productAndServices[data-v-a17563fc]{background-color:#fff}.ri-productAndServices ul[data-v-a17563fc]{display:flex;list-style:none;padding-left:0}@media (max-width:767.98px){.ri-productAndServices ul[data-v-a17563fc]{flex-wrap:wrap}}.ri-product__card[data-v-a17563fc]{flex:1;position:relative;margin-right:40px;margin-left:40px}.ri-product__card:hover p[data-v-a17563fc]{color:#3d3d3d}.ri-product__card:hover .ri-subTitle[data-v-a17563fc]{transition:.5s;color:#00b3b6}.ri-product__card:hover .ri-more__link[data-v-a17563fc]{color:#00b3b6;text-decoration:none}.ri-product__card:hover .ri-more__link[data-v-a17563fc]:after{transition:.5s;width:90px}.ri-product__card[data-v-a17563fc]:first-child{margin-left:0}.ri-product__card[data-v-a17563fc]:last-child{margin-right:0}.ri-product__card[data-v-a17563fc]:last-child:after{display:none}.ri-product__card[data-v-a17563fc]:after{content:\"\";position:absolute;top:0;right:-40px;border-left:1px solid #d6d6d6;height:100%}@media (max-width:991.98px){.ri-product__card[data-v-a17563fc]{margin-right:20px;margin-left:20px}.ri-product__card[data-v-a17563fc]:after{right:-20px}}@media (max-width:767.98px){.ri-product__card[data-v-a17563fc]{flex:auto;width:100%;margin-right:0;margin-left:0;margin-bottom:40px}.ri-product__card[data-v-a17563fc]:last-child{margin-bottom:0}.ri-product__card[data-v-a17563fc]:after{display:none}}.ri-product__card__img[data-v-a17563fc]{position:relative;margin-bottom:40px;padding-bottom:75%;height:0;overflow:hidden}.ri-product__card__img img[data-v-a17563fc]{position:absolute;left:0;top:0;width:100%;height:100%;-o-object-fit:cover;object-fit:cover}@media (max-width:767.98px){.ri-product__card__img[data-v-a17563fc]{margin-bottom:20px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/ProductAndServices.vue?vue&type=template&id=a17563fc&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-productAndServices ri-section"},[_vm._ssrNode("<div class=\"container\" data-v-a17563fc>","</div>",[_vm._ssrNode("<h2 class=\"ri-mainTitle\" data-v-a17563fc><span data-v-a17563fc>"+_vm._ssrEscape(_vm._s(_vm.$t('navigation.product')))+"</span></h2> "),_c('client-only',[_c('ul',{attrs:{"data-aos":"zoom-in"}},_vm._l((_vm.$store.state.GlobalArticleData.product),function(data,index){return _c('li',{key:index,staticClass:"ri-product__card"},[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath({name: ("" + (data.Slug))})}},[_c('div',{staticClass:"ri-product__card__img"},[_c('img',{attrs:{"src":(data.LanguageData[_vm.$i18n.locale].Cover || data.Cover),"alt":""}})]),_vm._v(" "),_c('div',{staticClass:"ri-product__card__text"},[_c('h3',{staticClass:"ri-subTitle"},[_vm._v("\n                "+_vm._s(data.LanguageData[_vm.$i18n.locale].Name || data.Name)+"\n              ")]),_vm._v(" "),_c('p',[_vm._v("\n                "+_vm._s(data.LanguageData[_vm.$i18n.locale].Introduction || data.Introduction)+"\n              ")]),_vm._v(" "),_c('a',{staticClass:"ri-more",attrs:{"href":"javascript:;"}},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                  "+_vm._s(_vm.$t('global.more'))+"\n                ")])])])])],1)}),0)]),_vm._ssrNode(" "),_vm._ssrNode("<template data-v-a17563fc>","</template>",[_vm._ssrNode("<ul class=\"d-none\" data-v-a17563fc>","</ul>",_vm._l((_vm.$store.state.GlobalArticleData.product),function(data,index){return _vm._ssrNode("<li class=\"ri-product__card\" data-v-a17563fc>","</li>",[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath({name: ("" + (data.Slug))})}},[_c('div',{staticClass:"ri-product__card__img"},[_c('img',{attrs:{"src":(data.LanguageData[_vm.$i18n.locale].Cover || data.Cover),"alt":""}})]),_vm._v(" "),_c('div',{staticClass:"ri-product__card__text"},[_c('h3',{staticClass:"ri-subTitle"},[_vm._v("\n                "+_vm._s(data.LanguageData[_vm.$i18n.locale].Name || data.Name)+"\n              ")]),_vm._v(" "),_c('p',[_vm._v("\n                "+_vm._s(data.LanguageData[_vm.$i18n.locale].Introduction || data.Introduction)+"\n              ")]),_vm._v(" "),_c('a',{staticClass:"ri-more",attrs:{"href":"javascript:;"}},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                  "+_vm._s(_vm.$t('global.more'))+"\n                ")])])])])],1)}),0)])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/index/ProductAndServices.vue?vue&type=template&id=a17563fc&scoped=true&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/index/ProductAndServices.vue

var script = {}
function injectStyles (context) {
  
  var style0 = __webpack_require__(163)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "a17563fc",
  "42afbd7d"
  
)

/* harmony default export */ var ProductAndServices = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index-product-and-services.js.map
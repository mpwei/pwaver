exports.ids = [0];
exports.modules = {

/***/ 105:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(152);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("18f44292", content, true, context)
};

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAndNews_vue_vue_type_style_index_0_id_997d863c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(105);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAndNews_vue_vue_type_style_index_0_id_997d863c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAndNews_vue_vue_type_style_index_0_id_997d863c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAndNews_vue_vue_type_style_index_0_id_997d863c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAndNews_vue_vue_type_style_index_0_id_997d863c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 152:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-aboutAndNews[data-v-997d863c]{background-color:#f3f3f3}.ri-mainTitle[data-v-997d863c]{text-align:left;color:#395265}.ri-mainTitle span[data-v-997d863c]{border-color:#395265}@media (max-width:767.98px){.ri-mainTitle[data-v-997d863c]{text-align:center}}.ri-aboutAndNews__group[data-v-997d863c]{display:flex}@media (max-width:767.98px){.ri-aboutAndNews__group[data-v-997d863c]{flex-wrap:wrap}}.ri-about[data-v-997d863c]{color:#395265;flex:1}.ri-about ul[data-v-997d863c]{display:flex;flex-wrap:wrap;list-style:none;padding-left:0;margin-bottom:-40px}@media (max-width:767.98px){.ri-about[data-v-997d863c]{flex:auto;margin-bottom:60px}}.ri-about__card[data-v-997d863c]{width:calc(50% - 20px);margin-bottom:40px}.ri-about__card[data-v-997d863c]:nth-child(odd){margin-right:20px}.ri-about__card[data-v-997d863c]:nth-child(2n){margin-left:20px}.ri-about__card>a[data-v-997d863c]{display:block;transition:.5s;border:1px solid #395265;padding:50px 20px;position:relative;height:100%}.ri-about__card>a span[data-v-997d863c]{transition:.5s;top:-20px;position:absolute;font-size:30px;background-color:#f3f3f3;padding:0 10px}.ri-about__card>a h3[data-v-997d863c]{transition:.5s;font-size:26px}.ri-about__card:hover>a[data-v-997d863c]{transition:.5s;border:1px solid #00b3b6}.ri-about__card:hover>a span[data-v-997d863c],.ri-about__card:hover h3[data-v-997d863c]{transition:.5s;color:#00b3b6}@media (min-width:992px) and (max-width:1439.98px){.ri-about__card>a span[data-v-997d863c]{transition:.5s;top:-20px;position:absolute;font-size:28px;padding:0 10px}.ri-about__card>a h3[data-v-997d863c]{font-size:24px}}@media (max-width:991.98px){.ri-about__card[data-v-997d863c]{width:calc(50% - 10px)}.ri-about__card[data-v-997d863c]:nth-child(odd){margin-right:10px}.ri-about__card[data-v-997d863c]:nth-child(2n){margin-left:10px}.ri-about__card>a[data-v-997d863c]{display:block;transition:.5s;padding:40px 16px;position:relative}.ri-about__card>a span[data-v-997d863c]{transition:.5s;top:-20px;position:absolute;font-size:30px;padding:0 10px}.ri-about__card>a h3[data-v-997d863c]{font-size:20px}}.ri-news[data-v-997d863c]{display:flex;flex-direction:column;justify-content:space-between;color:#395265;flex:1}.ri-news ul[data-v-997d863c]{list-style:none;padding-left:0}.ri-news__list[data-v-997d863c]{transform:scaleX(1);transform-origin:bottom right;margin-bottom:20px;padding-bottom:5px;border-bottom:1px solid #395265}.ri-news__list .ri-a[data-v-997d863c]{display:flex;justify-content:space-between}.ri-news__list p[data-v-997d863c]{font-size:24px;display:flex;font-weight:300;margin-bottom:0}.ri-news__list p .date[data-v-997d863c]{flex-shrink:0;font-style:italic;margin-right:20px}.ri-news__list[data-v-997d863c]:hover{color:#00b3b6;border-bottom:1px solid #00b3b6;transition:.5s;transform:scaleX(1.05);transform-origin:bottom right}.ri-news__list:hover .ri-more__link[data-v-997d863c]{color:#00b3b6}@media (max-width:1199.98px){.ri-news__list p[data-v-997d863c]{font-size:16px}.ri-news__list .ri-more[data-v-997d863c]{display:none}}@media (max-width:767.98px){.ri-news__list p[data-v-997d863c]{font-size:14px}.ri-news__list[data-v-997d863c]:hover{transform:scaleX(1)}}.ri-line[data-v-997d863c]{border-left:1px solid #395265;margin:0 100px}@media (min-width:1200px) and (max-width:1439.98px){.ri-line[data-v-997d863c]{margin:0 60px}}@media (max-width:1199.98px){.ri-line[data-v-997d863c]{margin:0 40px}}@media (max-width:767.98px){.ri-line[data-v-997d863c]{display:none}}.ri-more[data-v-997d863c]{display:inline-block;margin-top:0;margin-right:3px}.ri-more[data-v-997d863c],.ri-more__link[data-v-997d863c]{color:#395265}.ri-more__link[data-v-997d863c]:hover{color:#00b3b6;text-decoration:none}.ri-more__link[data-v-997d863c]:hover:after{transition:.4s;width:100px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/index/AboutAndNews.vue?vue&type=template&id=997d863c&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-aboutAndNews ri-section"},[_vm._ssrNode("<div class=\"container\" data-v-997d863c>","</div>",[_vm._ssrNode("<div class=\"ri-aboutAndNews__group\" data-v-997d863c>","</div>",[_c('client-only',[_c('div',{staticClass:"ri-about",attrs:{"data-aos":"fade-right"}},[_c('h2',{staticClass:"ri-mainTitle"},[_c('span',[_vm._v(_vm._s(_vm.$t('navigation.about')))])]),_vm._v(" "),_c('ul',_vm._l((_vm.$store.state.GlobalArticleData.aboutUS),function(data,index){return _c('li',{key:index,staticClass:"ri-about__card"},[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath(("/aboutUS#ri-" + (data.Slug)))}},[_c('span',{class:data.Variable.Icon}),_vm._v(" "),_c('h3',[_vm._v(_vm._s(data.LanguageData[_vm.$i18n.locale].Name || data.Name))])])],1)}),0)])]),_vm._ssrNode(" "),_vm._ssrNode("<template data-v-997d863c>","</template>",[_vm._ssrNode("<div class=\"ri-about d-none\" data-v-997d863c>","</div>",[_vm._ssrNode("<h2 class=\"ri-mainTitle\" data-v-997d863c><span data-v-997d863c>"+_vm._ssrEscape(_vm._s(_vm.$t('navigation.about')))+"</span></h2> "),_vm._ssrNode("<ul data-v-997d863c>","</ul>",_vm._l((_vm.$store.state.GlobalArticleData.aboutUS),function(data,index){return _vm._ssrNode("<li class=\"ri-about__card\" data-v-997d863c>","</li>",[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath(("/aboutUS#ri-" + (data.Slug)))}},[_c('span',{class:data.Variable.Icon}),_vm._v(" "),_c('h3',[_vm._v(_vm._s(data.LanguageData[_vm.$i18n.locale].Name || data.Name))])])],1)}),0)],2)]),_vm._ssrNode(" <div class=\"ri-line\" data-v-997d863c></div> "),_c('client-only',[_c('div',{staticClass:"ri-news",attrs:{"data-aos":"fade-left"}},[_c('div',{},[_c('h2',{staticClass:"ri-mainTitle"},[_c('span',[_vm._v(_vm._s(_vm.$t('navigation.news')))])]),_vm._v(" "),_c('ul',_vm._l((_vm.$store.state.GlobalArticleData.news),function(data,index){return _c('li',{key:index,staticClass:"ri-news__list"},[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath(("/news/" + (data.Slug)))}},[_c('p',[_c('span',{staticClass:"date"},[_vm._v(_vm._s(_vm.$dayjs(data.CreateTime).format('YYYY-MM-DD')))]),_vm._v(" "),_c('span',[_vm._v(_vm._s(data.LanguageData[_vm.$i18n.locale].Name || data.Name))])]),_vm._v(" "),_c('a',{staticClass:"ri-more",attrs:{"href":"javascript:;"}},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                      "+_vm._s(_vm.$t('global.more'))+"\n                    ")])])])],1)}),0)]),_vm._v(" "),_c('div',{staticClass:"text-center text-md-right"},[_c('nuxt-link',{staticClass:"ri-button ri-button--blue",attrs:{"to":_vm.localePath('/news')}},[_vm._v("\n              "+_vm._s(_vm.$t('global.viewDetail'))+"\n            ")])],1)])]),_vm._ssrNode(" "),_vm._ssrNode("<template data-v-997d863c>","</template>",[_vm._ssrNode("<div class=\"ri-news d-none\" data-v-997d863c>","</div>",[_vm._ssrNode("<div data-v-997d863c>","</div>",[_vm._ssrNode("<h2 class=\"ri-mainTitle\" data-v-997d863c><span data-v-997d863c>"+_vm._ssrEscape(_vm._s(_vm.$t('navigation.news')))+"</span></h2> "),_vm._ssrNode("<ul data-v-997d863c>","</ul>",_vm._l((_vm.$store.state.GlobalArticleData.news),function(data,index){return _vm._ssrNode("<li class=\"ri-news__list\" data-v-997d863c>","</li>",[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath(("/news/" + (data.Slug)))}},[_c('p',[_c('span',{staticClass:"date"},[_vm._v(_vm._s(_vm.$dayjs(data.CreateTime).format('YYYY-MM-DD')))]),_vm._v(" "),_c('span',[_vm._v(_vm._s(data.LanguageData[_vm.$i18n.locale].Name || data.Name))])]),_vm._v(" "),_c('a',{staticClass:"ri-more",attrs:{"href":"javascript:;"}},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                      "+_vm._s(_vm.$t('global.more'))+"\n                    ")])])])],1)}),0)],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"text-center text-md-right\" data-v-997d863c>","</div>",[_c('nuxt-link',{staticClass:"ri-button ri-button--blue",attrs:{"to":_vm.localePath('/news')}},[_vm._v("\n              "+_vm._s(_vm.$t('global.viewDetail'))+"\n            ")])],1)],2)])],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/index/AboutAndNews.vue?vue&type=template&id=997d863c&scoped=true&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/index/AboutAndNews.vue

var script = {}
function injectStyles (context) {
  
  var style0 = __webpack_require__(151)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "997d863c",
  "3ee05f54"
  
)

/* harmony default export */ var AboutAndNews = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index-about-and-news.js.map
exports.ids = [7];
exports.modules = {

/***/ 28:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  navigation: {
    index: 'Home',
    '3DSpace': '3D Space Model',
    about: 'About',
    news: 'Event',
    product: 'Product & Service',
    download: 'Download',
    contact: 'Contact'
  },
  index: {
    title: 'Home',
    customerTitle: 'Customer',
    fieldCaseTitle: 'Field Case'
  },
  contact: {
    title: 'Contact',
    formPlaceholder: {
      name: 'Name',
      email: 'Email',
      phone: 'Phone',
      message: 'Message',
      send: 'Send'
    }
  },
  global: {
    messages: {
      process: 'Process...',
      sendMailSuccess: 'Success to send message, please remind you email and phone, and we will dedicate to serve you.',
      sendMailError: 'Fail to send message!!'
    },
    more: 'More',
    viewDetail: 'View Detail'
  },
  download: {
    downloadSystemMenu: {
      name: 'Download System Menu'
    },
    downloadTestData: {
      name: 'Download Test Data'
    },
    downloadDetect: {
      name: 'Download Detect'
    },
    button: 'Download'
  },
  footer: {
    siteMap: 'SiteMap',
    address: 'Address',
    tel: 'Phone',
    fax: 'Fax',
    email: 'Email',
    language: {
      zh: 'Ch',
      en: 'En',
      ja: 'Jp'
    }
  }
});

/***/ })

};;
//# sourceMappingURL=lang-en-us.js.map
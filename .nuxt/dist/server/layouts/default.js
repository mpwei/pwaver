exports.ids = [19,12,13,17];
exports.modules = {

/***/ 103:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(148);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("56b15182", content, true, context)
};

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Header.vue?vue&type=template&id=05e28c18&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('b-navbar',{staticClass:"ri-header",attrs:{"toggleable":"xl","type":"light","variant":"light"}},[_c('div',{staticClass:"container"},[_c('b-navbar-brand',{staticClass:"ri-header__logo",attrs:{"to":_vm.localePath('/')}},[_c('h1',[_c('img',{staticClass:"mr-3",attrs:{"src":"/image/P-WAVER-Logo.svg","alt":"P-WAVER"}}),_vm._v("P-WAVER")])]),_vm._v(" "),_c('b-navbar-toggle',{staticClass:"ri-header__button",attrs:{"target":"nav-collapse"}},[_c('span',{staticClass:"icon-Menu-1"}),_vm._v(" "),_c('span',{staticClass:"icon-Menu"})]),_vm._v(" "),_c('b-collapse',{attrs:{"id":"nav-collapse","is-nav":""}},[_c('b-navbar-nav',{staticClass:"ml-auto py-4"},[_c('b-nav-item',{attrs:{"to":_vm.localePath('/')}},[_c('span',[_vm._v(_vm._s(_vm.$t('navigation.index')))])]),_vm._v(" "),_c('b-nav-item-dropdown',{attrs:{"text":_vm.$t('navigation.about')}},[_vm._l((_vm.$store.state.GlobalArticleData.aboutUS),function(content,index){return [_c('b-dropdown-item',{key:index,attrs:{"to":_vm.localePath(("/aboutUS#ri-" + (content.Slug)))}},[_c('span',[_vm._v(_vm._s(content.LanguageData[_vm.$i18n.locale].Name))])])]})],2),_vm._v(" "),_c('b-nav-item',{attrs:{"to":_vm.localePath('/news')}},[_c('span',[_vm._v(_vm._s(_vm.$t('navigation.news')))])]),_vm._v(" "),_c('b-nav-item-dropdown',{attrs:{"text":_vm.$t('navigation.product')}},[_vm._l((_vm.$store.state.GlobalArticleData.product),function(content,index){return [_c('b-dropdown-item',{key:index,attrs:{"to":_vm.localePath(("/" + (content.Slug)))}},[_c('span',[_vm._v(_vm._s(content.LanguageData[_vm.$i18n.locale].Name))])])]})],2),_vm._v(" "),_c('b-nav-item-dropdown',{attrs:{"text":_vm.$t('navigation.download')}},[_c('b-dropdown-item',{attrs:{"to":_vm.localePath('/downloadSystemMenu')}},[_c('span',[_vm._v(_vm._s(_vm.$t('download.downloadSystemMenu.name')))])]),_vm._v(" "),_c('b-dropdown-item',{attrs:{"to":_vm.localePath('/downloadTestData')}},[_c('span',[_vm._v(_vm._s(_vm.$t('download.downloadTestData.name')))])]),_vm._v(" "),_c('b-dropdown-item',{attrs:{"to":_vm.localePath('/downloadDetect')}},[_c('span',[_vm._v(_vm._s(_vm.$t('download.downloadDetect.name')))])])],1),_vm._v(" "),_c('b-nav-item',{attrs:{"to":_vm.localePath('/contact')}},[_c('span',[_vm._v(_vm._s(_vm.$t('navigation.contact')))])]),_vm._v(" "),_c('b-navbar-nav',{staticClass:"ri-header__language"},[_c('b-nav-item',{on:{"click":function($event){return _vm.$i18n.setLocale('zh-tw')}}},[_c('span',[_vm._v(_vm._s(_vm.$t('footer.language.zh')))])]),_vm._v(" "),_c('b-nav-item',{on:{"click":function($event){return _vm.$i18n.setLocale('en-us')}}},[_c('span',[_vm._v(_vm._s(_vm.$t('footer.language.en')))])])],1)],1)],1)],1)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/Header.vue?vue&type=template&id=05e28c18&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Header.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Headervue_type_script_lang_js_ = ({
  data() {
    return {};
  },

  mounted() {},

  methods: {}
});
// CONCATENATED MODULE: ./components/Header.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Headervue_type_script_lang_js_ = (Headervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/Header.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(81)
if (style0.__inject__) style0.__inject__(context)
var style1 = __webpack_require__(83)
if (style1.__inject__) style1.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_Headervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "05e28c18",
  "0f1deacc"
  
)

/* harmony default export */ var Header = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ScrollTop.vue?vue&type=template&id=51fc3b24&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-scrollTop",on:{"click":function($event){return _vm.scrollToTop()}}},[_vm._ssrNode("<div class=\"ri-scrollTop__bg\"></div> "),_c('b-icon',{staticClass:"ri-scrollTop__arrow",attrs:{"icon":"chevron-up","aria-hidden":"true"}})],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ScrollTop.vue?vue&type=template&id=51fc3b24&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ScrollTop.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
/* harmony default export */ var ScrollTopvue_type_script_lang_js_ = ({
  data() {
    return {
      bottom: 20
    };
  },

  methods: {
    scrollToTop() {
      // 至頂端
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    }

  }
});
// CONCATENATED MODULE: ./components/ScrollTop.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ScrollTopvue_type_script_lang_js_ = (ScrollTopvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/ScrollTop.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(85)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ScrollTopvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "69cb9dcb"
  
)

/* harmony default export */ var ScrollTop = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Footer.vue?vue&type=template&id=3ec8688b&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.loadShow),expression:"loadShow"}],staticClass:"ri-footer"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"ri-footer__info\">","</div>",[_vm._ssrNode("<div class=\"ri-footer__info__logo\"><img"+(_vm._ssrAttr("src",_vm.$store.state.Meta.LanguageData[_vm.$i18n.locale].Favicon || _vm.$store.state.Meta.Favicon))+(_vm._ssrAttr("alt",_vm.$store.state.Meta.LanguageData[_vm.$i18n.locale].Title || _vm.$store.state.Meta.Title))+"></div> "),_vm._ssrNode("<div class=\"ri-footer__info__map\">","</div>",[_vm._ssrNode("<h4>"+_vm._ssrEscape(" "+_vm._s(_vm.$t('footer.siteMap')))+"</h4> "),_vm._ssrNode("<ul>","</ul>",[_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/aboutUS')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.about'))+"\n            ")])],1),_vm._ssrNode(" "),_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/news')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.news'))+"\n            ")])],1),_vm._ssrNode(" "),_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/productBefore')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.product'))+"\n            ")])],1),_vm._ssrNode(" "),_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/downloadSystemMenu')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.download'))+"\n            ")])],1),_vm._ssrNode(" "),_vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":_vm.localePath('/contact')}},[_vm._v("\n              "+_vm._s(_vm.$t('navigation.contact'))+"\n            ")])],1),_vm._ssrNode(" <li><ul class=\"ri-footer__info__map__group\"><li><a href=\"javascript:void(0)\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.$t('footer.language.zh'))+"\n                ")+"</a></li> <li><a href=\"javascript:void(0)\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.$t('footer.language.en'))+"\n                ")+"</a></li></ul></li>")],2)],2),_vm._ssrNode(" <div class=\"ri-footer__info__contact\"><h4 class=\"d-none d-md-block\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.$t('navigation.contact'))+"\n        ")+"</h4> <h4 class=\"d-block d-md-none\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.Contact.CompanyName)+"\n        ")+"</h4> <ul><li class=\"ri-footer__info__contact__address\">"+_vm._ssrEscape("\n            "+_vm._s(_vm.$t('footer.address'))+": "+_vm._s(_vm.Contact.Address)+"\n          ")+"</li> <li><a href=\"tel:+886-2-6630-5187\">"+_vm._ssrEscape(_vm._s(_vm.$t('footer.tel'))+": "+_vm._s(_vm.Contact.Tel))+"</a></li> <li class=\"ri-footer__info__contact__email\"><a"+(_vm._ssrAttr("href",_vm.Contact.Email))+">"+_vm._ssrEscape(_vm._s(_vm.$t('footer.email'))+": "+_vm._s(_vm.Contact.Email))+"</a></li> <li>"+_vm._ssrEscape(_vm._s(_vm.$t('footer.fax'))+": "+_vm._s(_vm.Contact.Fax))+"</li></ul></div> <div class=\"ri-footer__info__media\"><ul><li><a"+(_vm._ssrAttr("href",_vm.InstagramLink))+" target=\"_blank\" class=\"ri-a\""+(_vm._ssrStyle(null,null, { display: (_vm.InstagramLink) ? '' : 'none' }))+"><span class=\"icon-instagram\"></span></a></li> <li><a"+(_vm._ssrAttr("href",_vm.TwitterLink))+" target=\"_blank\" class=\"ri-a\""+(_vm._ssrStyle(null,null, { display: (_vm.TwitterLink) ? '' : 'none' }))+"><span class=\"icon-twitter\"></span></a></li> <li><a"+(_vm._ssrAttr("href",_vm.FBLink))+" target=\"_blank\" class=\"ri-a\""+(_vm._ssrStyle(null,null, { display: (_vm.FBLink) ? '' : 'none' }))+"><span class=\"icon-facebook\"></span></a></li> <li><a href=\"/\" class=\"ri-a\"><span class=\"icon-web\"></span></a></li></ul></div>")],2),_vm._ssrNode(" <div class=\"ri-footer__copyright\"><p>"+_vm._ssrEscape("Copyright © "+_vm._s(_vm.$store.state.Meta.LanguageData[_vm.$i18n.locale].Title || _vm.$store.state.Meta.Title)+" All Rights Reserved.")+"</p></div>")],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/Footer.vue?vue&type=template&id=3ec8688b&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Footer.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Footervue_type_script_lang_js_ = ({
  data() {
    return {
      loadShow: false,
      FBLink: '',
      TwitterLink: '',
      InstagramLink: '',
      Contact: {}
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.LoadAllLanguage();
    }

  },

  mounted() {
    this.loadShow = true;
    this.LoadAllLanguage();
    this.FBLink = this.$store.state.Social.find(item => {
      return item.Name === 'Facebook';
    }).Link;
    this.TwitterLink = this.$store.state.Social.find(item => {
      return item.Name === 'Twitter';
    }).Link;
    this.InstagramLink = this.$store.state.Social.find(item => {
      return item.Name === 'Instagram';
    }).Link;
  },

  methods: {
    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    },

    LoadAllLanguage() {
      this.Contact = this.DetermineLanguage(this.$store.state.Contact);
    }

  }
});
// CONCATENATED MODULE: ./components/Footer.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Footervue_type_script_lang_js_ = (Footervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/Footer.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(87)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_Footervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "5182b2b0"
  
)

/* harmony default export */ var Footer = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_default_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(103);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_default_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_default_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_default_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_default_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 148:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "html{font-family:\"Source Sans Pro\",-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,sans-serif;font-size:16px;word-spacing:1px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;box-sizing:border-box}*,:after,:before{box-sizing:border-box;margin:0}.button--green{display:inline-block;border-radius:4px;border:1px solid #3b8070;color:#3b8070;text-decoration:none;padding:10px 30px}.button--green:hover{color:#fff;background-color:#3b8070}.button--grey{display:inline-block;border-radius:4px;border:1px solid #35495e;color:#35495e;text-decoration:none;padding:10px 30px;margin-left:15px}.button--grey:hover{color:#fff;background-color:#35495e}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./layouts/default.vue?vue&type=template&id=cbcaf33c&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Header'),_vm._ssrNode(" "),_c('Nuxt'),_vm._ssrNode(" "),_c('ScrollTop'),_vm._ssrNode(" "),_c('Footer')],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./layouts/default.vue?vue&type=template&id=cbcaf33c&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./layouts/default.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var defaultvue_type_script_lang_js_ = ({
  components: {
    Header: () => Promise.resolve(/* import() */).then(__webpack_require__.bind(null, 114)),
    ScrollTop: () => Promise.resolve(/* import() */).then(__webpack_require__.bind(null, 115)),
    Footer: () => Promise.resolve(/* import() */).then(__webpack_require__.bind(null, 116))
  }
});
// CONCATENATED MODULE: ./layouts/default.vue?vue&type=script&lang=js&
 /* harmony default export */ var layouts_defaultvue_type_script_lang_js_ = (defaultvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./layouts/default.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(147)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  layouts_defaultvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "036bf295"
  
)

/* harmony default export */ var layouts_default = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {Header: __webpack_require__(114).default,ScrollTop: __webpack_require__(115).default,Footer: __webpack_require__(116).default})


/***/ }),

/***/ 75:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(82);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("47cadc0b", content, true, context)
};

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(84);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("2da68355", content, true, context)
};

/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(86);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("5566865e", content, true, context)
};

/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(88);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("33164fa7", content, true, context)
};

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(75);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".navbar-light .navbar-nav .nav-link{color:#3d3d3d;font-weight:300;padding-left:20px;padding-right:20px;padding-bottom:0}.navbar-light .navbar-nav .nav-link:focus,.navbar-light .navbar-nav .nav-link:hover{color:#36679c}.navbar-light .navbar-nav .nav-link:focus span,.navbar-light .navbar-nav .nav-link:hover span{position:relative;display:inline-block}.navbar-light .navbar-nav .nav-link:focus span:before,.navbar-light .navbar-nav .nav-link:hover span:before{content:\"\";width:100%;position:absolute;border-bottom:2px solid #36679c;left:50%;transform:translate(-50%);bottom:-2px}@media (max-width:1199.98px){.navbar-light .navbar-nav{text-align:center}.navbar-light .navbar-nav .nav-link{margin:5px 0}}@media (max-width:767.98px){.navbar-light .navbar-nav .nav-link{margin:2px 0}}.navbar-collapse{min-height:0!important;transition:.5s}.navbar-collapse.show{min-height:100vh!important}.dropdown-menu{text-align:center;left:50%;transform:translate(-50%);font-size:22px;border:none;box-shadow:0 3px 6px rgba(0,0,0,.2);padding-bottom:20px;margin-top:25px}.dropdown-menu .dropdown-item{font-weight:300;position:relative}.dropdown-menu .dropdown-item:hover{color:#36679c;background:transparent}.dropdown-menu .dropdown-item:hover span{position:relative;display:inline-block}.dropdown-menu .dropdown-item:hover span:before{content:\"\";width:100%;position:absolute;border-bottom:2px solid #36679c;left:50%;transform:translate(-50%);bottom:-2px}@media (min-width:1440px) and (max-width:1599.98px){.dropdown-menu{font-size:20px}}@media (min-width:1200px) and (max-width:1439.98px){.dropdown-menu{font-size:16px;margin-top:18px}}@media (max-width:1199.98px){.dropdown-menu{left:0;font-size:20px;transform:translate(0);box-shadow:none;background:rgba(54,103,156,.2);margin-top:5px}}@media (max-width:767.98px){.dropdown-menu{font-size:16px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(76);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_1_id_05e28c18_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-header[data-v-05e28c18]{position:fixed;top:0;width:100%;padding:1px 1rem;font-size:26px;box-shadow:0 3px 6px rgba(0,0,0,.2);z-index:99}@media (min-width:1440px) and (max-width:1599.98px){.ri-header[data-v-05e28c18]{font-size:22px}}@media (min-width:1200px) and (max-width:1439.98px){.ri-header[data-v-05e28c18]{font-size:18px}}@media (min-width:768px) and (max-width:1200px){.ri-header[data-v-05e28c18]{font-size:22px}}@media (max-width:767.98px){.ri-header[data-v-05e28c18]{font-size:18px}}.ri-header__button[data-v-05e28c18]{color:#3d3d3d;border:none}.ri-header__button[data-v-05e28c18]:hover{color:#36679c}.ri-header__button .icon-Menu[data-v-05e28c18],.ri-header__button[aria-expanded=true] .icon-Menu-1[data-v-05e28c18]{display:none}.ri-header__button[aria-expanded=true] .icon-Menu[data-v-05e28c18]{display:block}.ri-header__logo[data-v-05e28c18]{padding:0;font-size:0;width:172px;white-space:nowrap;overflow:hidden}.ri-header__logo h1[data-v-05e28c18]{margin-bottom:0}.ri-header__logo img[data-v-05e28c18]{width:100%}@media (max-width:1439.98px){.ri-header__logo[data-v-05e28c18]{width:120px}}@media (max-width:991.98px){.ri-header__logo[data-v-05e28c18]{width:100px}}.ri-header__language[data-v-05e28c18]{display:flex}.ri-header__language .nav-item:last-child .nav-link[data-v-05e28c18]:after{display:none}.ri-header__language .nav-link[data-v-05e28c18]{position:relative;padding-left:10px;padding-right:10px}.ri-header__language .nav-link[data-v-05e28c18]:hover:before{width:calc(100% - 20px)}.ri-header__language .nav-link[data-v-05e28c18]:after{content:\"/\";position:absolute;right:-4px}@media (max-width:1200px){.ri-header__language[data-v-05e28c18]{flex-direction:row;justify-content:center}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollTop_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(77);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollTop_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollTop_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollTop_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollTop_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-scrollTop{position:fixed;right:10px;bottom:10px;z-index:98;color:#fff;transition:.5s;cursor:pointer;font-size:36px}.ri-scrollTop:hover .ri-scrollTop__bg{transition:.5s;opacity:.5}.ri-scrollTop:hover .ri-scrollTop__arrow{transition:.5s;transform:translate(-50%,-60%)}.ri-scrollTop .ri-scrollTop__bg{width:70px;height:70px;background-color:#00b3b6;box-shadow:0 3px 6px rgba(0,0,0,.2);border-radius:50%;transition:.5s}.ri-scrollTop .ri-scrollTop__arrow{position:absolute;z-index:99;top:50%;left:50%;transform:translate(-50%,-50%);transition:.5s}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(78);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-footer{background-color:#3d3d3d}.ri-footer__info{display:flex;justify-content:space-between;color:#fff}@media (max-width:767.98px){.ri-footer__info{display:block}}.ri-footer__info__logo{flex-shrink:0;width:200px}@media (min-width:1200px) and (max-width:1599.98px){.ri-footer__info__logo{width:150px}}@media (max-width:767.98px){.ri-footer__info__logo{margin:0 auto -40px}}.ri-footer__info__map{padding:40px 0;max-width:600px}.ri-footer__info__map h4{font-size:24px;margin-bottom:20px}.ri-footer__info__map ul{list-style:none;padding-left:0;display:flex;flex-wrap:wrap;margin-bottom:-10px}.ri-footer__info__map ul li{width:25%;margin-bottom:10px}.ri-footer__info__map ul li a{color:#fff}.ri-footer__info__map ul li a:hover{color:#00b3b6}.ri-footer__info__map .ri-footer__info__map__group{flex-wrap:nowrap}.ri-footer__info__map .ri-footer__info__map__group li{width:auto}.ri-footer__info__map .ri-footer__info__map__group li:last-child:after{display:none}.ri-footer__info__map .ri-footer__info__map__group li:after{content:\"/\";margin:0 2px}@media (min-width:1200px) and (max-width:1599.98px){.ri-footer__info__map h4{font-size:22px;margin-bottom:20px}.ri-footer__info__map ul li{width:33.3%}}@media (min-width:768px) and (max-width:1199.98px){.ri-footer__info__map h4{font-size:22px;margin-bottom:20px}.ri-footer__info__map ul li{width:50%}}@media (max-width:767.98px){.ri-footer__info__map{display:none}}.ri-footer__info__contact{padding:40px 0;max-width:600px}.ri-footer__info__contact h4{font-size:24px;margin-bottom:20px}.ri-footer__info__contact ul{list-style:none;padding-left:0;display:flex;flex-wrap:wrap;margin-bottom:-10px}.ri-footer__info__contact ul li{margin-bottom:10px}.ri-footer__info__contact ul li a{color:#fff}.ri-footer__info__contact ul li a:hover{color:#00b3b6}.ri-footer__info__contact .ri-footer__info__contact__address,.ri-footer__info__contact .ri-footer__info__contact__email{flex-grow:1}@media (min-width:1200px) and (max-width:1599.98px){.ri-footer__info__contact h4{font-size:22px;margin-bottom:20px}.ri-footer__info__contact ul{display:block}}@media (max-width:1199.98px){.ri-footer__info__contact h4{font-size:22px;margin-bottom:20px}.ri-footer__info__contact ul{display:block}}@media (max-width:767.98px){.ri-footer__info__contact{padding:20px 0;text-align:center}}.ri-footer__info__media{padding:40px 0}.ri-footer__info__media ul{list-style:none;padding-left:0;display:flex;margin-bottom:0}.ri-footer__info__media ul li{padding:0 8px}@media (max-width:991.98px){.ri-footer__info__media ul{list-style:none;padding-left:0;display:flex;flex-wrap:wrap;margin-bottom:0}.ri-footer__info__media ul li{width:100%;text-align:right;padding:0 8px}}@media (max-width:767.98px){.ri-footer__info__media{padding:0 0 20px}.ri-footer__info__media ul{justify-content:center}.ri-footer__info__media ul li{width:auto}}.ri-footer__copyright{border-top:1px solid #fff;padding-top:20px;padding-bottom:40px;text-align:center;color:#fff}@media (max-width:767.98px){.ri-footer__copyright{padding-bottom:20px}}.ri-a:hover{color:#00b3b6}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=default.js.map
exports.ids = [21];
exports.modules = {

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(91);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 124:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "#ri-about__development,#ri-about__service,#ri-about__team,#ri-about__technology{position:relative;top:-140px}.ri-contact{background-color:#f3f3f3}.ri-contact .ri-subTitle{color:#395265;margin-bottom:0}.ri-contact__form__group{position:relative;padding-top:40px}.ri-contact__form__group label{color:#395265;position:absolute;top:40px;left:0;height:1.5rem;transition:.4s;padding:0 5px;font-size:24px}@media (max-width:1199.98px){.ri-contact__form__group label{top:46px;font-size:16px}}@media (max-width:767.98px){.ri-contact__form__group label{font-size:14px}}.ri-contact__form__group input.form-control{color:#395265;min-height:40px;padding-left:80px;border-radius:0;border:none;border-bottom:1px solid #395265;background-color:transparent}.ri-contact__form__group input.form-control:focus{box-shadow:none;border:none;border-bottom:1px solid #00b3b6}.ri-contact__form__group input.form-control:focus+label{color:#00b3b6;background-color:transparent;transition:.4s}.ri-contact__form__group textarea.form-control{padding:16px;color:#395265;border-radius:0;border:1px solid #395265;background-color:transparent}.ri-contact__form__group textarea.form-control::-moz-placeholder{color:#395265;font-size:24px}.ri-contact__form__group textarea.form-control:-ms-input-placeholder{color:#395265;font-size:24px}.ri-contact__form__group textarea.form-control::placeholder{color:#395265;font-size:24px}@media (max-width:1199.98px){.ri-contact__form__group textarea.form-control::-moz-placeholder{font-size:16px}.ri-contact__form__group textarea.form-control:-ms-input-placeholder{font-size:16px}.ri-contact__form__group textarea.form-control::placeholder{font-size:16px}}@media (max-width:767.98px){.ri-contact__form__group textarea.form-control::-moz-placeholder{font-size:14px}.ri-contact__form__group textarea.form-control:-ms-input-placeholder{font-size:14px}.ri-contact__form__group textarea.form-control::placeholder{font-size:14px}}.ri-contact__form__group textarea.form-control:focus{box-shadow:none;border:1px solid #00b3b6}.ri-contact__form__group textarea.form-control:focus::-moz-placeholder{color:#00b3b6}.ri-contact__form__group textarea.form-control:focus:-ms-input-placeholder{color:#00b3b6}.ri-contact__form__group textarea.form-control:focus::placeholder{color:#00b3b6}.ri-contact__form__group .invalid-feedback{font-size:18px;bottom:-30px;position:absolute;color:#dd5765;padding-left:5px!important}@media (max-width:1199.98px){.ri-contact__form__group .invalid-feedback{font-size:16px}}@media (max-width:767.98px){.ri-contact__form__group .invalid-feedback{font-size:14px}}.ri-contact__form__group .is-invalid,.ri-contact__form__group .is-valid{background-image:none}.ri-aboutUS__form__button{font-weight:700;font-size:1.125rem;padding:14px auto;border-radius:15px;width:100%}.ri-contact__content__map{background-color:silver;position:relative;height:100%}.ri-contact__content__map iframe{width:100%;height:100%;position:absolute;top:0}@media (max-width:767.98px){.ri-contact__content__map{height:400px}}.ri-alert{width:50%;text-align:center;position:fixed;top:120px;left:50%;transform:translateX(-50%);z-index:2}@media (max-width:575.98px){.ri-alert{width:80%}}@media (max-width:767.98px){.ri-alert{top:85px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/contact.vue?vue&type=template&id=272a5824&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-layout"},[_vm._ssrNode("<div class=\"container ri-container\">","</div>",[_vm._ssrNode("<div class=\"d-none d-md-block ri-breadCrumb\">","</div>",[_c('nuxt-link',{attrs:{"to":""}},[_vm._v("\n        "+_vm._s(_vm.$t('contact.title'))+"\n      ")])],1),_vm._ssrNode(" <div class=\"d-block d-md-none ri-breadCrumb\"><h2 class=\"ri-mainTitle\"><span>"+_vm._ssrEscape(_vm._s(_vm.$t('contact.title')))+"</span></h2></div>")],2),_vm._ssrNode(" "),_vm._ssrNode("<div>","</div>",[_vm._ssrNode("<div class=\"ri-contact ri-section\">","</div>",[_vm._ssrNode("<span id=\"ri-contact\"></span> "),_vm._ssrNode("<div class=\"container ri-container\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div data-aos=\"fade-right\" class=\"col-12 col-md-6\">","</div>",[_vm._ssrNode("<h2 class=\"ri-subTitle ri-subTitle--content\">"+_vm._ssrEscape("\n              "+_vm._s(_vm.$t('contact.title'))+"\n            ")+"</h2> "),_c('ValidationObserver',{ref:"form",scopedSlots:_vm._u([{key:"default",fn:function(ref){
var handleSubmit = ref.handleSubmit;
return [_c('b-form',{staticClass:"ri-contact__form",on:{"submit":function($event){$event.preventDefault();return handleSubmit(_vm.onSubmit)}}},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12"},[_c('div',{staticClass:"ri-contact__form__group"},[_c('validation-provider',{attrs:{"name":_vm.$t('contact.formPlaceholder.name'),"rules":"required"},scopedSlots:_vm._u([{key:"default",fn:function(validationContext){return [_c('b-form-input',{attrs:{"id":"input-1","type":"text","autocomplete":"off","state":_vm.getValidationState(validationContext)},model:{value:(_vm.form.name),callback:function ($$v) {_vm.$set(_vm.form, "name", $$v)},expression:"form.name"}}),_vm._v(" "),_c('label',{attrs:{"for":"input-1"}},[_vm._v(_vm._s(_vm.$t('contact.formPlaceholder.name')))]),_vm._v(" "),_c('b-form-invalid-feedback',{staticClass:"px-2"},[_vm._v("\n                          "+_vm._s(validationContext.errors[0])+"\n                        ")])]}}],null,true)})],1)]),_vm._v(" "),_c('div',{staticClass:"col-12"},[_c('div',{staticClass:"ri-contact__form__group"},[_c('validation-provider',{attrs:{"name":_vm.$t('contact.formPlaceholder.email'),"rules":"required|email"},scopedSlots:_vm._u([{key:"default",fn:function(validationContext){return [_c('b-form-input',{attrs:{"id":"input-3","type":"email","autocomplete":"off","state":_vm.getValidationState(validationContext)},model:{value:(_vm.form.email),callback:function ($$v) {_vm.$set(_vm.form, "email", $$v)},expression:"form.email"}}),_vm._v(" "),_c('label',{attrs:{"for":"input-3"}},[_vm._v(_vm._s(_vm.$t('contact.formPlaceholder.email')))]),_vm._v(" "),_c('b-form-invalid-feedback',{staticClass:"px-2"},[_vm._v("\n                          "+_vm._s(validationContext.errors[0])+"\n                        ")])]}}],null,true)})],1)]),_vm._v(" "),_c('div',{staticClass:"col-12"},[_c('div',{staticClass:"ri-contact__form__group"},[_c('validation-provider',{attrs:{"name":_vm.$t('contact.formPlaceholder.phone'),"rules":"required"},scopedSlots:_vm._u([{key:"default",fn:function(validationContext){return [_c('b-form-input',{attrs:{"id":"input-2","type":"tel","autocomplete":"off","state":_vm.getValidationState(validationContext)},model:{value:(_vm.form.phone),callback:function ($$v) {_vm.$set(_vm.form, "phone", $$v)},expression:"form.phone"}}),_vm._v(" "),_c('label',{attrs:{"for":"input-2"}},[_vm._v(_vm._s(_vm.$t('contact.formPlaceholder.phone')))]),_vm._v(" "),_c('b-form-invalid-feedback',{staticClass:"px-2"},[_vm._v("\n                          "+_vm._s(validationContext.errors[0])+"\n                        ")])]}}],null,true)})],1)]),_vm._v(" "),_c('div',{staticClass:"col-12 "},[_c('div',{staticClass:"ri-contact__form__group"},[_c('b-form-textarea',{attrs:{"id":"input-7","rows":"3","autocomplete":"off","placeholder":_vm.$t('contact.formPlaceholder.message')},model:{value:(_vm.form.message),callback:function ($$v) {_vm.$set(_vm.form, "message", $$v)},expression:"form.message"}})],1)])]),_vm._v(" "),_c('div',{staticClass:"text-center text-md-right mt-5"},[_c('button',{class:[_vm.disableButton? 'ri-button ri-button--disable':'ri-button ri-button--blue'],attrs:{"type":"submit"}},[_vm._v("\n                    "+_vm._s(_vm.$t('contact.formPlaceholder.send'))+"\n                  ")])])])]}}])})],2),_vm._ssrNode(" <div data-aos=\"fade-left\" class=\"col-12 col-md-6\"><div class=\"d-none d-md-block ri-contact__content__map\"><iframe"+(_vm._ssrAttr("src",'https://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q='+ _vm.$store.state.Contact.Address +'&z=16&output=embed&t='))+" width=\"600\" height=\"450\" allowfullscreen=\"allowfullscreen\" loading=\"lazy\" style=\"border:0;\"></iframe></div></div>")],2)])],2),_vm._ssrNode(" <div class=\"d-block d-md-none ri-contact__content__map\"><iframe"+(_vm._ssrAttr("src",'https://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q='+ _vm.$store.state.Contact.Address +'&z=16&output=embed&t='))+" width=\"600\" height=\"450\" allowfullscreen=\"allowfullscreen\" loading=\"lazy\" style=\"border:0;\"></iframe></div>")],2),_vm._ssrNode(" "),_c('b-alert',{staticClass:"ri-alert",attrs:{"show":_vm.alert,"variant":_vm.alertType}},[_vm._v("\n    "+_vm._s(_vm.alertMessage)+"\n  ")])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/contact.vue?vue&type=template&id=272a5824&

// EXTERNAL MODULE: external "vee-validate"
var external_vee_validate_ = __webpack_require__(8);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/contact.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var contactvue_type_script_lang_js_ = ({
  name: 'Contact',
  components: {
    ValidationProvider: external_vee_validate_["ValidationProvider"],
    ValidationObserver: external_vee_validate_["ValidationObserver"]
  },

  data() {
    return {
      form: {
        name: '',
        email: '',
        phone: '',
        message: ''
      },
      disableButton: false,
      alert: false,
      alertType: 'danger',
      alertMessage: '',
      Meta: {
        Title: 'P-Waver 國家地震中心'
      }
    };
  },

  head() {
    return {
      title: `${this.$t('contact.title')} | ${this.Meta.Title}`,
      meta: [{
        hid: 'description',
        name: 'description',
        content: this.Meta.Description
      }, {
        property: 'og:description',
        content: this.Meta.Description
      }, {
        name: 'keywords',
        content: this.Meta.Keywords
      }, {
        property: 'og:title',
        content: `${this.$t('contact.title')} | ${this.Meta.Title}`
      }, {
        property: 'og:type',
        content: 'article'
      }]
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.Meta = this.DetermineLanguage(this.Meta);
    }

  },

  mounted() {
    Object(external_vee_validate_["localize"])(this.$i18n.locale);
    this.Meta = this.DetermineLanguage(this.$store.state.Meta);
  },

  methods: {
    alertHandler(message, type, time = 2000) {
      this.alert = true;
      this.alertType = type;
      this.alertMessage = message;
      setTimeout(() => {
        this.alert = false;
      }, time);
      return Promise.resolve();
    },

    getValidationState({
      dirty,
      validated,
      valid = null
    }) {
      return dirty || validated ? valid : null;
    },

    onSubmit() {
      this.disableButton = true;
      this.alertHandler(this.$t('global.messages.process'), 'info', 5000);
      return this.$axios.request({
        method: 'post',
        url: '/api/message/notify/mail',
        headers: {
          'Content-Type': 'application/json'
        },
        data: { ...this.form,
          token: this.token
        }
      }).then(() => {
        this.alertHandler(this.$t('global.messages.sendMailSuccess'), 'success', 5000);
        this.form = {
          name: '',
          email: '',
          phone: '',
          message: ''
        };
        this.$refs.form.reset();
        this.disableButton = false;
      }).catch(e => {
        console.log(e);
        this.alertHandler(this.$t('global.messages.sendMailError'), 'danger', 5000);
      });
    },

    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/contact.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_contactvue_type_script_lang_js_ = (contactvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./pages/contact.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(123)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_contactvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "2a58356f"
  
)

/* harmony default export */ var contact = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 91:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(124);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("96c36aca", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=contact.js.map
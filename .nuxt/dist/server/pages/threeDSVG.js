exports.ids = [31,11,18];
exports.modules = {

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/threeDSVG.vue?vue&type=template&id=e0d982fc&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-layout"},[_c('ClassTitle',{attrs:{"class-title":_vm.ClassTitle,"language-title":_vm.LanguageTitle,"single-data":_vm.LanguageData}}),_vm._ssrNode(" "),_c('ThreeDInner',{attrs:{"single-data":_vm.SingleData,"language-data":_vm.LanguageData}})],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/threeDSVG.vue?vue&type=template&id=e0d982fc&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/threeDSVG.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
/* harmony default export */ var threeDSVGvue_type_script_lang_js_ = ({
  async asyncData({
    $axios,
    redirect,
    app
  }) {
    let LanguageTitle, LanguageData;
    const ClassTitle = await $axios.post('/api/category', {
      Slug: 'threeD'
    }).then(({
      data
    }) => {
      return data.Data;
    }).catch(error => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(error));
      redirect('/error');
    });
    const SingleData = await $axios.get('/api/article/detail/threeDSVG').then(({
      data
    }) => {
      if (data.Data === null) {
        return redirect('/error');
      }

      return data.Data;
    }).catch(error => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(error));
      redirect('/error');
    });

    if (app.i18n.locale && ClassTitle.LanguageData) {
      LanguageTitle = ClassTitle.LanguageData[`${app.i18n.locale}`];
    } else {
      LanguageTitle = ClassTitle;
    }

    if (app.i18n.locale && SingleData.LanguageData) {
      LanguageData = SingleData.LanguageData[`${app.i18n.locale}`];
    } else {
      LanguageData = SingleData;
    }

    return {
      ClassTitle,
      LanguageTitle,
      LanguageData,
      SingleData
    };
  },

  data() {
    return {
      Meta: {
        Title: 'P-Waver 國家地震中心'
      }
    };
  },

  head() {
    return {
      title: `${this.LanguageData.Name} | ${this.Meta.Title}`,
      meta: [{
        hid: 'description',
        name: 'description',
        content: this.LanguageData.SEO.Description
      }, {
        property: 'og:description',
        content: this.LanguageData.SEO.Description
      }, {
        name: 'keywords',
        content: this.LanguageData.SEO.Keywords
      }, {
        property: 'og:title',
        content: `${this.LanguageData.Name} | ${this.Meta.Title}`
      }, {
        property: 'og:image',
        content: `${this.LanguageData.Cover}`
      }, {
        property: 'og:type',
        content: 'article'
      }]
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.Meta = this.DetermineLanguage(this.Meta);
    }

  },

  mounted() {
    this.Meta = this.DetermineLanguage(this.$store.state.Meta);
  },

  methods: {
    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/threeDSVG.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_threeDSVGvue_type_script_lang_js_ = (threeDSVGvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./pages/threeDSVG.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_threeDSVGvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "742e3aa6"
  
)

/* harmony default export */ var threeDSVG = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {ClassTitle: __webpack_require__(69).default,ThreeDInner: __webpack_require__(73).default})


/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=template&id=458214ec&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container ri-container"},[_vm._ssrNode("<div class=\"d-none d-md-block ri-breadCrumb\">","</div>",[(_vm.classTitle.Slug === 'threeD')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ThreeDSpace')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'product')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ProductAndServices')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'download')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#Download')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):_c('nuxt-link',{attrs:{"to":_vm.localePath(("/" + (_vm.classTitle.Slug)))}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]),_vm._ssrNode(" "),_c('b-icon',{attrs:{"icon":"chevron-right","aria-hidden":"true"}}),_vm._ssrNode(" "),_c('nuxt-link',{attrs:{"to":""}},[_vm._v("\n      "+_vm._s(_vm.singleData.Name)+"\n    ")])],2),_vm._ssrNode(" <div class=\"d-block d-md-none ri-breadCrumb\"><h2 class=\"ri-mainTitle\"><span>"+_vm._ssrEscape(_vm._s(_vm.classTitle.Name))+"</span></h2></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=template&id=458214ec&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ClassTitlevue_type_script_lang_js_ = ({
  props: {
    classTitle: {
      type: Object,
      default: () => ({})
    },
    languageTitle: {
      type: Object,
      default: () => ({})
    },
    singleData: {
      type: Object,
      default: null
    }
  }
});
// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ClassTitlevue_type_script_lang_js_ = (ClassTitlevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/ClassTitle.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ClassTitlevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "36d007cd"
  
)

/* harmony default export */ var ClassTitle = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 70:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(72);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("933fbd5e", content, true, context)
};

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDInner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(70);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDInner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDInner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDInner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ThreeDInner_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-space{margin-bottom:140px}@media (min-width:1200px) and (max-width:1439.98px){.ri-space{margin-bottom:80px}}@media (min-width:992px) and (max-width:1199.98px){.ri-space{margin-bottom:60px}}@media (max-width:991.98px){.ri-space{margin-bottom:60px}}.ri-space__carousel{position:absolute}.ri-space__carousel .carousel-inner{max-height:540px;overflow:hidden}.ri-space__carousel .carousel-item{height:0;padding-bottom:75%}.ri-space__carousel .carousel-item img{position:absolute;height:100%;-o-object-fit:cover;object-fit:cover;-o-object-position:center center;object-position:center center}@media (max-width:1199.98px){.ri-space__carousel{position:relative;margin-bottom:80px}}.ri-space__text{min-height:540px}.ri-space__text .ri-space__text__content{padding-left:44px}@media (max-width:1199.98px){.ri-space__text{min-height:auto}.ri-space__text .ri-space__text__content{padding-left:16px}}.ri-space .carousel-control-next{display:block;position:absolute;top:auto;bottom:-40px;color:#3d3d3d;right:150px;z-index:98}.ri-space .carousel-control-next .carousel-control-next-icon{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='=3D3D' width='8' height='8'%3E%3Cpath d='M2.75 0l-1.5 1.5L3.75 4l-2.5 2.5L2.75 8l4-4-4-4z'/%3E%3C/svg%3E\")}.ri-space .carousel-control-prev{display:block;position:absolute;top:auto;bottom:-40px;left:150px;z-index:98}.ri-space .carousel-control-prev .carousel-control-prev-icon{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='=3D3D' width='8' height='8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5L4.25 4l2.5-2.5L5.25 0z'/%3E%3C/svg%3E\")}.ri-space .carousel-indicators{bottom:-40px;margin-bottom:0}.ri-space .carousel-indicators li{width:15px;height:15px;border-radius:50%;background-color:#3d3d3d}.ri-space .carousel-indicators li.active{background-color:#00b3b6}@media (max-width:767.98px){.ri-space .carousel-indicators li{width:10px;height:10px}}@media (max-width:767.98px){.ri-space .carousel-control-next{right:100px}.ri-space .carousel-control-prev{left:100px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ThreeDInner.vue?vue&type=template&id=5f7312a0&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{},[_c('client-only',[_c('div',{staticClass:"ri-space",attrs:{"data-aos":"fade-up"}},[_c('div',{staticClass:"position-relative"},[_c('div',{staticClass:"ri-space__carousel container-fluid"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12 col-xl-6 pl-0 pr-0 pr-xl-3"},[_c('b-carousel',{attrs:{"id":"carousel-1","interval":4000,"controls":"","indicators":"","background":"#e6e9ed"},on:{"sliding-start":_vm.onSlideStart,"sliding-end":_vm.onSlideEnd},model:{value:(_vm.slide),callback:function ($$v) {_vm.slide=$$v},expression:"slide"}},_vm._l((_vm.languageData.Carousel),function(carouselItem,index){return _c('b-carousel-slide',{key:carouselItem.Alt + index,attrs:{"img-src":carouselItem.URL,"img-alt":carouselItem.Alt}})}),1)],1)])]),_vm._v(" "),_c('div',{staticClass:"ri-space__text container ri-container"},[_c('div',{staticClass:"row justify-content-end"},[_c('div',{staticClass:"ri-space__text__content col-12 col-xl-6"},[_c('h2',{staticClass:"ri-subTitle ri-subTitle--content"},[_vm._v("\n                "+_vm._s(_vm.languageData.Name)+"\n              ")]),_vm._v(" "),_c('div',[(_vm.singleData.EnableResponseContent === 1)?[_c('div',{staticClass:"d-none d-lg-block ck-content",domProps:{"innerHTML":_vm._s(_vm.languageData.ResponseData.Web.Content)}}),_vm._v(" "),_c('div',{staticClass:"d-block d-lg-none ck-content",domProps:{"innerHTML":_vm._s(_vm.languageData.ResponseData.Mobile.Content)}})]:(_vm.singleData.EnableResponseContent === 0)?[_c('div',{staticClass:"ck-content",domProps:{"innerHTML":_vm._s(_vm.languageData.Content)}})]:_vm._e()],2)])])])])])]),_vm._ssrNode(" "),_vm._ssrNode("<template>","</template>",[_vm._ssrNode("<div class=\"ri-space d-none\">","</div>",[_vm._ssrNode("<div class=\"position-relative\">","</div>",[_vm._ssrNode("<div class=\"ri-space__carousel container-fluid\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-12 col-xl-6 pl-0 pr-0 pr-xl-3\">","</div>",[_c('b-carousel',{attrs:{"id":"carousel-1","interval":4000,"controls":"","indicators":"","background":"#e6e9ed"},on:{"sliding-start":_vm.onSlideStart,"sliding-end":_vm.onSlideEnd},model:{value:(_vm.slide),callback:function ($$v) {_vm.slide=$$v},expression:"slide"}},_vm._l((_vm.languageData.Carousel),function(carouselItem,index){return _c('b-carousel-slide',{key:carouselItem.Alt + index,attrs:{"img-src":carouselItem.URL,"img-alt":carouselItem.Alt}})}),1)],1)])]),_vm._ssrNode(" <div class=\"ri-space__text container ri-container\"><div class=\"row justify-content-end\"><div class=\"ri-space__text__content col-12 col-xl-6\"><h2 class=\"ri-subTitle ri-subTitle--content\">"+_vm._ssrEscape("\n                "+_vm._s(_vm.languageData.Name)+"\n              ")+"</h2> <div>"+((_vm.singleData.EnableResponseContent === 1)?("<div class=\"d-none d-lg-block ck-content\">"+(_vm._s(_vm.languageData.ResponseData.Web.Content))+"</div> <div class=\"d-block d-lg-none ck-content\">"+(_vm._s(_vm.languageData.ResponseData.Mobile.Content))+"</div>"):(_vm.singleData.EnableResponseContent === 0)?("<div class=\"ck-content\">"+(_vm._s(_vm.languageData.Content))+"</div>"):"<!---->")+"</div></div></div></div>")],2)])])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ThreeDInner.vue?vue&type=template&id=5f7312a0&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ThreeDInner.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ThreeDInnervue_type_script_lang_js_ = ({
  props: {
    singleData: {
      type: Object,
      default: null
    },
    languageData: {
      type: Object,
      default: null
    }
  },

  data() {
    return {
      slide: 0,
      sliding: null
    };
  },

  methods: {
    onSlideStart(slide) {
      this.sliding = true;
    },

    onSlideEnd(slide) {
      this.sliding = false;
    }

  }
});
// CONCATENATED MODULE: ./components/ThreeDInner.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ThreeDInnervue_type_script_lang_js_ = (ThreeDInnervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/ThreeDInner.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(71)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ThreeDInnervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "ea77115a"
  
)

/* harmony default export */ var ThreeDInner = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=threeDSVG.js.map
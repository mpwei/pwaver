exports.ids = [26,11];
exports.modules = {

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(146);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("00c7237b", content, true, context)
};

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_id_vue_vue_type_style_index_0_id_e7ef19ee_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(102);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_id_vue_vue_type_style_index_0_id_e7ef19ee_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_id_vue_vue_type_style_index_0_id_e7ef19ee_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_id_vue_vue_type_style_index_0_id_e7ef19ee_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_id_vue_vue_type_style_index_0_id_e7ef19ee_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 146:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-newsPage[data-v-e7ef19ee]{margin-bottom:100px}@media (min-width:1200px) and (max-width:1439.98px){.ri-newsPage[data-v-e7ef19ee]{margin-bottom:80px}}@media (max-width:1199.98px){.ri-newsPage[data-v-e7ef19ee]{margin-bottom:60px}}.ri-newsPage__content[data-v-e7ef19ee]{border:1px solid #3d3d3d;padding:40px}.ri-newsPage__content .ri-subTitle[data-v-e7ef19ee]{display:flex;justify-content:space-between;align-items:flex-end}.ri-newsPage__content .ri-subTitle span[data-v-e7ef19ee]{font-weight:400;font-size:16px;color:#3d3d3d}.ri-newsPage__content img[data-v-e7ef19ee]{width:100%;margin-bottom:16px}.ri-newsPage__content .ri-button[data-v-e7ef19ee]{margin-top:40px}.ri-newsPage__team__card[data-v-e7ef19ee]{width:calc(33% - 26.6px);margin-bottom:40px;border:1px solid #3d3d3d;margin-left:20px;margin-right:20px}.ri-newsPage__team__card[data-v-e7ef19ee]:nth-child(3n){margin-right:0}.ri-newsPage__team__card[data-v-e7ef19ee]:nth-child(3n+1){margin-left:0}@media (max-width:1199.98px){.ri-newsPage__team__card[data-v-e7ef19ee]{width:calc(50% - 20px);margin-left:20px;margin-right:20px}.ri-newsPage__team__card[data-v-e7ef19ee]:nth-child(3n){margin-right:20px}.ri-newsPage__team__card[data-v-e7ef19ee]:nth-child(3n+1){margin-left:20px}.ri-newsPage__team__card[data-v-e7ef19ee]:nth-child(odd){margin-left:0}.ri-newsPage__team__card[data-v-e7ef19ee]:nth-child(2n){margin-right:0}}@media (max-width:767.98px){.ri-newsPage__team__card[data-v-e7ef19ee]{width:100%;margin-left:0;margin-right:0}.ri-newsPage__team__card[data-v-e7ef19ee]:nth-child(3n){margin-right:0}.ri-newsPage__team__card[data-v-e7ef19ee]:nth-child(3n+1){margin-left:0}}.ri-newsPage__team__card[data-v-e7ef19ee]:hover{border:1px solid #00b3b6;box-shadow:0 3px 6px rgba(0,0,0,.2);transition:.4s}.ri-newsPage__team__card:hover .ri-newsPage__team__card__text .ri-newsPage__team__card__text__date[data-v-e7ef19ee],.ri-newsPage__team__card:hover .ri-newsPage__team__card__text .ri-subTitle[data-v-e7ef19ee]{color:#00b3b6}.ri-newsPage__team__card:hover .ri-newsPage__team__card__text .ri-more__link[data-v-e7ef19ee]{color:#00b3b6;text-decoration:none}.ri-newsPage__team__card:hover .ri-newsPage__team__card__text .ri-more__link[data-v-e7ef19ee]:after,.ri-newsPage__team__card:hover .ri-newsPage__team__card__text .ri-more__link[data-v-e7ef19ee]:before{transition:.4s}.ri-newsPage__team__card__img[data-v-e7ef19ee]{position:relative;padding-bottom:75%;height:0;overflow:hidden}.ri-newsPage__team__card__img img[data-v-e7ef19ee]{position:absolute;left:0;top:0;width:100%;height:100%;-o-object-fit:cover;object-fit:cover}.ri-newsPage__team__card__text[data-v-e7ef19ee]{padding:40px}.ri-newsPage__team__card__text .ri-newsPage__team__card__text__date[data-v-e7ef19ee]{display:block;font-style:italic}.ri-newsPage__team__card__text p[data-v-e7ef19ee]{margin-bottom:0}@media (max-width:767.98px){.ri-newsPage__team__card__text[data-v-e7ef19ee]{padding:20px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/news/_id.vue?vue&type=template&id=e7ef19ee&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-layout"},[_vm._ssrNode("<div class=\"container ri-container\" data-v-e7ef19ee>","</div>",[_c('ClassTitle',{attrs:{"class-title":_vm.ClassTitle,"language-title":_vm.LanguageTitle,"single-data":_vm.LanguageData}}),_vm._ssrNode(" "),_c('client-only',[_c('div',{staticClass:"ri-newsPage",attrs:{"data-aos":"fade-up"}},[_c('div',{staticClass:"ri-newsPage__content"},[_c('h2',{staticClass:"ri-subTitle ri-subTitle--content"},[_vm._v("\n            "+_vm._s(_vm.LanguageData.Name)+"\n            "),_c('span',[_vm._v(_vm._s(_vm.$dayjs(_vm.SingleData.CreateTime).format('YYYY-MM-DD')))])]),_vm._v(" "),_c('div',[(_vm.SingleData.EnableResponseContent === 1)?[_c('div',{staticClass:"d-none d-lg-block ck-content",domProps:{"innerHTML":_vm._s(_vm.LanguageData.ResponseData.Web.Content)}}),_vm._v(" "),_c('div',{staticClass:"d-block d-lg-none ck-content",domProps:{"innerHTML":_vm._s(_vm.LanguageData.ResponseData.Mobile.Content)}})]:(_vm.SingleData.EnableResponseContent === 0)?[_c('div',{staticClass:"ck-content",domProps:{"innerHTML":_vm._s(_vm.LanguageData.Content)}})]:_vm._e()],2),_vm._v(" "),_c('div',{staticClass:"text-center"},[_c('nuxt-link',{staticClass:"ri-button",attrs:{"to":_vm.localePath('/news')}},[_vm._v("\n              回上一頁\n            ")])],1)])])]),_vm._ssrNode(" "),_vm._ssrNode("<template data-v-e7ef19ee>","</template>",[_vm._ssrNode("<div class=\"d-none ri-newsPage\" data-v-e7ef19ee>","</div>",[_vm._ssrNode("<div class=\"ri-newsPage__content\" data-v-e7ef19ee>","</div>",[_vm._ssrNode("<h2 class=\"ri-subTitle ri-subTitle--content\" data-v-e7ef19ee>"+_vm._ssrEscape("\n            "+_vm._s(_vm.LanguageData.Name)+"\n            ")+"<span data-v-e7ef19ee>"+_vm._ssrEscape(_vm._s(_vm.$dayjs(_vm.LanguageData.CreateTime).format('YYYY-MM-DD')))+"</span></h2> <div data-v-e7ef19ee>"+((_vm.SingleData.EnableResponseContent === 1)?("<div class=\"d-none d-lg-block ck-content\" data-v-e7ef19ee>"+(_vm._s(_vm.LanguageData.ResponseData.Web.Content))+"</div> <div class=\"d-block d-lg-none ck-content\" data-v-e7ef19ee>"+(_vm._s(_vm.LanguageData.ResponseData.Mobile.Content))+"</div>"):(_vm.SingleData.EnableResponseContent === 0)?("<div class=\"ck-content\" data-v-e7ef19ee>"+(_vm._s(_vm.LanguageData.Content))+"</div>"):"<!---->")+"</div> "),_vm._ssrNode("<div class=\"text-center\" data-v-e7ef19ee>","</div>",[_c('nuxt-link',{staticClass:"ri-button",attrs:{"to":_vm.localePath('/news')}},[_vm._v("\n              回上一頁\n            ")])],1)],2)])])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/news/_id.vue?vue&type=template&id=e7ef19ee&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/news/_id.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var _idvue_type_script_lang_js_ = ({
  layout: 'default',

  async asyncData({
    $axios,
    route,
    redirect,
    app
  }) {
    let LanguageTitle, LanguageData;
    const ClassTitle = await $axios.post('/api/category', {
      Slug: 'news'
    }).then(({
      data
    }) => {
      return data.Data;
    }).catch(() => {
      redirect('/404');
    });
    const SingleData = await $axios.get('/api/article/detail/' + route.params.id).then(({
      data
    }) => {
      if (data.Data === null) {
        return redirect('/404');
      }

      return data.Data;
    }).catch(() => {
      redirect('/404');
    });

    if (app.i18n.locale && ClassTitle.LanguageData) {
      LanguageTitle = ClassTitle.LanguageData[`${app.i18n.locale}`];
    } else {
      LanguageTitle = ClassTitle;
    }

    if (app.i18n.locale && SingleData.LanguageData) {
      LanguageData = SingleData.LanguageData[`${app.i18n.locale}`];
    } else {
      LanguageData = SingleData;
    }

    return {
      ClassTitle,
      LanguageTitle,
      LanguageData,
      SingleData
    };
  },

  data() {
    return {
      Meta: {
        Title: 'P-Waver 國家地震中心'
      }
    };
  },

  head() {
    return {
      title: `${this.LanguageData.Name} | ${this.Meta.Title}`,
      meta: [{
        hid: 'description',
        name: 'description',
        content: this.LanguageData.SEO.Description
      }, {
        property: 'og:description',
        content: this.LanguageData.SEO.Description
      }, {
        name: 'keywords',
        content: this.LanguageData.SEO.Keywords
      }, {
        property: 'og:title',
        content: `${this.LanguageData.Name} | ${this.Meta.Title}`
      }, {
        property: 'og:image',
        content: `${this.LanguageData.Cover}`
      }, {
        property: 'og:type',
        content: 'article'
      }]
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.Meta = this.DetermineLanguage(this.Meta);
    }

  },

  mounted() {
    this.Meta = this.DetermineLanguage(this.$store.state.Meta);
  },

  methods: {
    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/news/_id.vue?vue&type=script&lang=js&
 /* harmony default export */ var news_idvue_type_script_lang_js_ = (_idvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./pages/news/_id.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(145)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  news_idvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "e7ef19ee",
  "8873711c"
  
)

/* harmony default export */ var _id = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {ClassTitle: __webpack_require__(69).default})


/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=template&id=458214ec&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container ri-container"},[_vm._ssrNode("<div class=\"d-none d-md-block ri-breadCrumb\">","</div>",[(_vm.classTitle.Slug === 'threeD')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ThreeDSpace')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'product')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ProductAndServices')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'download')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#Download')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):_c('nuxt-link',{attrs:{"to":_vm.localePath(("/" + (_vm.classTitle.Slug)))}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]),_vm._ssrNode(" "),_c('b-icon',{attrs:{"icon":"chevron-right","aria-hidden":"true"}}),_vm._ssrNode(" "),_c('nuxt-link',{attrs:{"to":""}},[_vm._v("\n      "+_vm._s(_vm.singleData.Name)+"\n    ")])],2),_vm._ssrNode(" <div class=\"d-block d-md-none ri-breadCrumb\"><h2 class=\"ri-mainTitle\"><span>"+_vm._ssrEscape(_vm._s(_vm.classTitle.Name))+"</span></h2></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=template&id=458214ec&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ClassTitlevue_type_script_lang_js_ = ({
  props: {
    classTitle: {
      type: Object,
      default: () => ({})
    },
    languageTitle: {
      type: Object,
      default: () => ({})
    },
    singleData: {
      type: Object,
      default: null
    }
  }
});
// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ClassTitlevue_type_script_lang_js_ = (ClassTitlevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/ClassTitle.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ClassTitlevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "36d007cd"
  
)

/* harmony default export */ var ClassTitle = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=_id.js.map
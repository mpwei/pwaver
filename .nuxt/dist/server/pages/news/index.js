exports.ids = [27];
exports.modules = {

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_409c8f38_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(96);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_409c8f38_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_409c8f38_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_409c8f38_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_409c8f38_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 134:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-news[data-v-409c8f38]{margin-bottom:100px}.ri-news>ul[data-v-409c8f38]{display:flex;list-style:none;padding-left:0;flex-wrap:wrap;margin-bottom:-40px}@media (min-width:1200px) and (max-width:1439.98px){.ri-news[data-v-409c8f38]{margin-bottom:80px}}@media (max-width:1199.98px){.ri-news[data-v-409c8f38]{margin-bottom:60px}}.ri-news__team__card[data-v-409c8f38]{width:calc(33% - 26.6px);margin-bottom:40px;border:1px solid #3d3d3d;margin-left:20px;margin-right:20px}.ri-news__team__card[data-v-409c8f38]:nth-child(3n){margin-right:0}.ri-news__team__card[data-v-409c8f38]:nth-child(3n+1){margin-left:0}@media (max-width:1199.98px){.ri-news__team__card[data-v-409c8f38]{width:calc(50% - 20px);margin-left:20px;margin-right:20px}.ri-news__team__card[data-v-409c8f38]:nth-child(3n){margin-right:20px}.ri-news__team__card[data-v-409c8f38]:nth-child(3n+1){margin-left:20px}.ri-news__team__card[data-v-409c8f38]:nth-child(odd){margin-left:0}.ri-news__team__card[data-v-409c8f38]:nth-child(2n){margin-right:0}}@media (max-width:767.98px){.ri-news__team__card[data-v-409c8f38]{width:100%;margin-left:0;margin-right:0}.ri-news__team__card[data-v-409c8f38]:nth-child(3n){margin-right:0}.ri-news__team__card[data-v-409c8f38]:nth-child(3n+1){margin-left:0}}.ri-news__team__card[data-v-409c8f38]:hover{border:1px solid #00b3b6;box-shadow:0 3px 6px rgba(0,0,0,.2);transition:.4s}.ri-news__team__card:hover .ri-news__team__card__text p[data-v-409c8f38]{color:#3d3d3d}.ri-news__team__card:hover .ri-news__team__card__text .ri-news__team__card__text__date[data-v-409c8f38],.ri-news__team__card:hover .ri-news__team__card__text .ri-subTitle[data-v-409c8f38]{color:#00b3b6}.ri-news__team__card:hover .ri-news__team__card__text .ri-more__link[data-v-409c8f38]{color:#00b3b6;text-decoration:none}.ri-news__team__card:hover .ri-news__team__card__text .ri-more__link[data-v-409c8f38]:after{transition:.4s;width:100px}.ri-news__team__card:hover .ri-news__team__card__text .ri-more__link[data-v-409c8f38]:before{transition:.4s}.ri-news__team__card__img[data-v-409c8f38]{position:relative;padding-bottom:75%;height:0;overflow:hidden}.ri-news__team__card__img img[data-v-409c8f38]{position:absolute;left:0;top:0;width:100%;height:100%;-o-object-fit:cover;object-fit:cover}.ri-news__team__card__text[data-v-409c8f38]{padding:40px}.ri-news__team__card__text .ri-news__team__card__text__date[data-v-409c8f38]{display:block;font-style:italic}.ri-news__team__card__text p[data-v-409c8f38]{font-size:22px;margin-bottom:0}@media (max-width:1199.98px){.ri-news__team__card__text p[data-v-409c8f38]{font-size:16px}}@media (max-width:767.98px){.ri-news__team__card__text[data-v-409c8f38]{padding:20px}.ri-news__team__card__text p[data-v-409c8f38]{font-size:14px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/news/index.vue?vue&type=template&id=409c8f38&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-layout"},[_vm._ssrNode("<div class=\"container ri-container\" data-v-409c8f38>","</div>",[_vm._ssrNode("<div class=\"d-none d-md-block ri-breadCrumb\" data-v-409c8f38>","</div>",[_c('nuxt-link',{attrs:{"to":""}},[_vm._v("\n        "+_vm._s(_vm.LanguageTitle.Name)+"\n      ")])],1),_vm._ssrNode(" <div class=\"d-block d-md-none ri-breadCrumb\" data-v-409c8f38><h2 class=\"ri-mainTitle\" data-v-409c8f38><span data-v-409c8f38>"+_vm._ssrEscape(_vm._s(_vm.LanguageTitle.Name))+"</span></h2></div> "),_c('client-only',[_c('div',{staticClass:"ri-news",attrs:{"data-aos":"fade-up"}},[_c('ul',_vm._l((_vm.Data),function(newsItem){return _c('li',{key:newsItem.Name,staticClass:"ri-news__team__card"},[_c('nuxt-link',{staticClass:"ri-a",attrs:{"to":_vm.localePath({name: 'news-id',params:{id: newsItem.Slug}})}},[_c('div',{staticClass:"ri-news__team__card__img"},[_c('img',{attrs:{"src":newsItem.Cover,"alt":newsItem.Name}})]),_vm._v(" "),_c('div',{staticClass:"ri-news__team__card__text"},[_c('span',{staticClass:"ri-news__team__card__text__date"},[_vm._v("\n                  "+_vm._s(_vm.$dayjs(newsItem.CreateTime).format('YYYY-MM-DD'))+"\n                ")]),_vm._v(" "),_c('h3',{staticClass:"ri-subTitle"},[_vm._v("\n                  "+_vm._s(newsItem.LanguageData[_vm.$i18n.locale].Name || newsItem.Name)+"\n                ")]),_vm._v(" "),_c('p',[_vm._v("\n                  "+_vm._s(newsItem.LanguageData[_vm.$i18n.locale].Introduction || newsItem.Introduction)+"\n                ")]),_vm._v(" "),_c('a',{staticClass:"ri-more",attrs:{"href":"javascript:;"}},[_c('span',{staticClass:"ri-more__link"},[_vm._v("\n                    "+_vm._s(_vm.$t('global.more'))+"\n                  ")])])])])],1)}),0),_vm._v(" "),_c('b-pagination-nav',{staticClass:"ri-pagination",attrs:{"pills":"","base-url":"#","next-text":"下一頁","prev-text":"上一頁","first-number":"","last-number":"","number-of-pages":_vm.Pagination.TotalPages,"per-page":_vm.Pagination.PerPage},on:{"input":_vm.ChangePage},model:{value:(_vm.Pagination.Page),callback:function ($$v) {_vm.$set(_vm.Pagination, "Page", $$v)},expression:"Pagination.Page"}})],1)])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/news/index.vue?vue&type=template&id=409c8f38&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/news/index.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var newsvue_type_script_lang_js_ = ({
  async asyncData({
    route,
    $axios,
    redirect,
    app
  }) {
    let LanguageTitle, LanguageData;
    const ClassTitle = await $axios.post('/api/category', {
      Slug: 'news'
    }).then(({
      data
    }) => {
      return data.Data;
    }).catch(error => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(error));
      redirect('/error');
    });
    const List = await $axios.post('/api/article/list', {
      Categories: ['news'],
      Page: route.query.page ? parseInt(route.query.page) : 1,
      PerPage: route.query.perPage ? parseInt(route.query.perPage) : 6,
      Order: route.query.order ? route.query.order : 'create_nto'
    }).then(({
      data
    }) => {
      return { ...data
      };
    }).catch(error => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(error));
      redirect('/error');
    });

    if (app.i18n.locale && ClassTitle.LanguageData) {
      LanguageTitle = ClassTitle.LanguageData[`${app.i18n.locale}`];
    } else {
      LanguageTitle = ClassTitle;
    }

    const everyPageLanguage = List.Data.every(item => {
      return item.LanguageData;
    }); // 確定都有多語系

    if (!everyPageLanguage) {
      console.error('有文章沒設定多語系');
    }

    if (app.i18n.locale && everyPageLanguage) {
      LanguageData = List.Data.map(item => {
        return item.LanguageData[`${app.i18n.locale}`];
      });
    } else {
      LanguageData = List;
    }

    console.log(List);
    return {
      LanguageTitle,
      LanguageData,
      ...List
    };
  },

  data() {
    return {
      Meta: {
        Title: 'P-Waver 國家地震中心'
      }
    };
  },

  head() {
    return {
      title: `${this.LanguageTitle.Name} | ${this.Meta.Title}`,
      meta: [{
        hid: 'description',
        name: 'description',
        content: this.LanguageTitle.SEO.Description
      }, {
        property: 'og:description',
        content: this.LanguageTitle.SEO.Description
      }, {
        name: 'keywords',
        content: this.LanguageTitle.SEO.Keywords
      }, {
        property: 'og:title',
        content: `${this.LanguageTitle.Name} | ${this.Meta.Title}`
      }, {
        property: 'og:image',
        content: `${this.LanguageTitle.Cover}`
      }]
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.Meta = this.DetermineLanguage(this.Meta);
    }

  },

  mounted() {
    this.Meta = this.DetermineLanguage(this.$store.state.Meta);
  },

  methods: {
    ChangePage() {
      this.$router.push({
        name: this.$route.name,
        query: {
          page: this.Pagination.Page,
          perPage: this.Pagination.PerPage,
          order: 'create_nto'
        }
      });
      return this.$axios.post('/api/article/list', {
        Categories: ['news'],
        Page: parseInt(this.Pagination.Page),
        PerPage: parseInt(this.Pagination.PerPage),
        Order: 'create_nto'
      }).then(({
        data
      }) => {
        this.Data = data.Data;
      });
    },

    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/news/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_newsvue_type_script_lang_js_ = (newsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./pages/news/index.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(133)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_newsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "409c8f38",
  "133b2a8a"
  
)

/* harmony default export */ var news = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 96:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(134);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("57230ef9", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=index.js.map
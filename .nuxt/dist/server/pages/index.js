exports.ids = [25,15];
exports.modules = {

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Loading.vue?vue&type=template&id=29f60e5a&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.loading),expression:"loading"}],staticClass:"loading-page"},[_vm._ssrNode("<div class=\"load-wrapp\"><div role=\"status\" class=\"load-circle spinner-border text-primary\"><span class=\"sr-only\">Loading...</span></div> <div class=\"load-word\"><div class=\"l-1 letter\">\n        L\n      </div> <div class=\"l-2 letter\">\n        o\n      </div> <div class=\"l-3 letter\">\n        a\n      </div> <div class=\"l-4 letter\">\n        d\n      </div> <div class=\"l-5 letter\">\n        i\n      </div> <div class=\"l-6 letter\">\n        n\n      </div> <div class=\"l-7 letter\">\n        g\n      </div> <div class=\"l-8 letter\">\n        .\n      </div> <div class=\"l-9 letter\">\n        .\n      </div> <div class=\"l-10 letter\">\n        .\n      </div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/Loading.vue?vue&type=template&id=29f60e5a&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Loading.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Loadingvue_type_script_lang_js_ = ({
  props: {
    loading: {
      type: Boolean,
      default: true
    }
  },
  watch: {
    loading: {
      // 深度监听，可监听到对象、数组的变化
      deep: true,
      immediate: true,

      handler(newV, oldV) {// console.log(newV)
      }

    }
  }
});
// CONCATENATED MODULE: ./components/Loading.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Loadingvue_type_script_lang_js_ = (Loadingvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/Loading.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(79)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_Loadingvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "782cdf02"
  
)

/* harmony default export */ var Loading = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(95);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 132:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "#__layout{overflow:hidden}.anchor{position:relative;top:-90px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=template&id=1d24a062&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-layout"},[_vm._ssrNode("<div>","</div>",[_c('Loading',{attrs:{"loading":_vm.loading}}),_vm._ssrNode(" "),_c('Banner',{attrs:{"carousel":_vm.Carousel}}),_vm._ssrNode(" "),_c('InformationData'),_vm._ssrNode(" "),_c('AboutAndNews'),_vm._ssrNode(" <span id=\"ProductAndServices\" class=\"anchor\"></span> "),_c('ProductAndServices'),_vm._ssrNode(" "),_c('FieldCase'),_vm._ssrNode(" "),_c('Customer',{attrs:{"carousel-bottom":_vm.CarouselBottom}}),_vm._ssrNode(" <span id=\"Download\" class=\"anchor\"></span> "),_c('Download')],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/index.vue?vue&type=template&id=1d24a062&

// EXTERNAL MODULE: ./plugins/axios.js
var axios = __webpack_require__(7);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var lib_vue_loader_options_pagesvue_type_script_lang_js_ = ({
  name: 'Index',
  components: {
    Loading: () => Promise.resolve(/* import() */).then(__webpack_require__.bind(null, 113)),
    Banner: () => __webpack_require__.e(/* import() */ 1).then(__webpack_require__.bind(null, 182)),
    InformationData: () => __webpack_require__.e(/* import() */ 5).then(__webpack_require__.bind(null, 185)),
    // ThreeDSpace: () => import('@/components/index/ThreeDSpace'),
    AboutAndNews: () => __webpack_require__.e(/* import() */ 0).then(__webpack_require__.bind(null, 187)),
    ProductAndServices: () => __webpack_require__.e(/* import() */ 6).then(__webpack_require__.bind(null, 189)),
    FieldCase: () => __webpack_require__.e(/* import() */ 4).then(__webpack_require__.bind(null, 184)),
    Customer: () => __webpack_require__.e(/* import() */ 2).then(__webpack_require__.bind(null, 183)),
    Download: () => __webpack_require__.e(/* import() */ 3).then(__webpack_require__.bind(null, 188))
  },
  layout: 'default',

  async asyncData({
    store,
    redirect
  }) {
    let Carousel, CarouselBottom;
    const GetCarousel = axios["a" /* request */].get('/api/themes/carousel').then(({
      data
    }) => {
      Carousel = data.Carousel;
      CarouselBottom = data.CarouselBottom;
      return {
        Carousel,
        CarouselBottom
      };
    }).catch(error => {
      throw error;
    });
    await Promise.all([GetCarousel, store.dispatch('LoadGlobalArticleData', {
      categories: 'aboutUS',
      order: 'sort_asc'
    }), store.dispatch('LoadGlobalArticleData', {
      categories: 'product',
      order: 'sort_asc'
    }), store.dispatch('LoadGlobalArticleData', {
      categories: 'news',
      order: 'create_nto'
    }), // store.dispatch('LoadGlobalArticleData', { categories: 'threeD' }),
    store.dispatch('LoadGlobalArticleData', {
      categories: 'fieldCase',
      order: 'sort_asc'
    })]).catch(() => {
      redirect('/error');
    });
    return {
      Carousel,
      CarouselBottom
    };
  },

  data() {
    return {
      loading: true,
      Carousel: [],
      CarouselBottom: [],
      Meta: {
        Title: '衛波科技股份有限公司 P-Waver'
      }
    };
  },

  head() {
    return {
      title: `${this.$t('index.title')} | ${this.Meta.Title}`,
      meta: [{
        hid: 'description',
        name: 'description',
        content: this.Meta.Description
      }, {
        property: 'og:description',
        content: this.Meta.Description
      }, {
        name: 'keywords',
        content: this.Meta.Keywords
      }, {
        property: 'og:title',
        content: `${this.$t('index.title')} | ${this.Meta.Title}`
      }, {
        property: 'og:type',
        content: 'article'
      }]
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.Meta = this.DetermineLanguage(this.$store.state.Meta);
    }

  },

  mounted() {
    this.Meta = this.DetermineLanguage(this.$store.state.Meta);
    this.loading = false;
  },

  methods: {
    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var pagesvue_type_script_lang_js_ = (lib_vue_loader_options_pagesvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./pages/index.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(131)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pagesvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "1c1e5a3e"
  
)

/* harmony default export */ var pages = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {Loading: __webpack_require__(113).default})


/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(80);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("a5bd365e", content, true, context)
};

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".loading-page{pointer-events:none;position:fixed;top:0;left:0;width:100%;height:100%;background:#fff;z-index:999;text-align:center;font-size:30px;font-family:sans-serif;transition:.5s;opacity:1}.loading-page--opacity{opacity:0;transition:.5s}.load-wrapp{position:relative;top:200px}.load-word{color:#00b3b6;display:flex;justify-content:center;align-items:center}.load-word .l-1{-webkit-animation-delay:.48s;animation-delay:.48s}.load-word .l-2{-webkit-animation-delay:.6s;animation-delay:.6s}.load-word .l-3{-webkit-animation-delay:.72s;animation-delay:.72s}.load-word .l-4{-webkit-animation-delay:.84s;animation-delay:.84s}.load-word .l-5{-webkit-animation-delay:.96s;animation-delay:.96s}.load-word .l-6{-webkit-animation-delay:1.08s;animation-delay:1.08s}.load-word .l-7{-webkit-animation-delay:1.2s;animation-delay:1.2s}.load-word .l-8{-webkit-animation-delay:1.32s;animation-delay:1.32s}.load-word .l-9{-webkit-animation-delay:1.44s;animation-delay:1.44s}.load-word .l-10{-webkit-animation-delay:1.56s;animation-delay:1.56s}.load-circle{margin-bottom:10px;border:4px solid #00b3b6;border-right-color:transparent}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 95:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(132);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("28f1127f", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=index.js.map
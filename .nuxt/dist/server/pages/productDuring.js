exports.ids = [30,11];
exports.modules = {

/***/ 100:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(142);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("4946c07b", content, true, context)
};

/***/ }),

/***/ 101:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(144);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("f60207cc", content, true, context)
};

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_0_id_3d90cc07_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(100);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_0_id_3d90cc07_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_0_id_3d90cc07_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_0_id_3d90cc07_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_0_id_3d90cc07_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 142:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-productFront[data-v-3d90cc07]{margin-bottom:100px}@media (min-width:1200px) and (max-width:1439.98px){.ri-productFront[data-v-3d90cc07]{margin-bottom:80px}}@media (max-width:1199.98px){.ri-productFront[data-v-3d90cc07]{margin-bottom:60px}}.ri-productFront__content .ri-subTitle[data-v-3d90cc07]{display:flex;justify-content:space-between;align-items:flex-end}.ri-productFront__content .ri-subTitle span[data-v-3d90cc07]{font-weight:400;font-size:16px;color:#3d3d3d}.ri-productFront__content .ri-button[data-v-3d90cc07]{margin-top:40px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_1_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(101);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_1_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_1_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_1_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productDuring_vue_vue_type_style_index_1_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 144:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-productFront__content__card{margin-bottom:40px}@media (max-width:991.98px){.ri-productFront__content__card{margin-bottom:0}}.ri-important{margin-bottom:20px}.ri-productFront__content__card__img{height:100%;width:100%;position:relative;overflow:hidden}.ri-productFront__content__card__img img{position:absolute;height:100%;width:100%;-o-object-fit:cover;object-fit:cover;-o-object-position:center;object-position:center}@media (max-width:991.98px){.ri-productFront__content__card__img{margin-bottom:20px}.ri-productFront__content__card__img img{position:relative;height:auto}}.ri-productFront__content__card__listGroup{display:flex;list-style:none;padding-left:0;flex-wrap:wrap;margin-bottom:0}.ri-productFront__content__card__listGroup__list{width:calc(33.33% - 40px);margin-bottom:40px;border:1px solid #3d3d3d;padding:40px;margin-left:30px;margin-right:30px}.ri-productFront__content__card__listGroup__list p{margin-bottom:0;height:24px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}.ri-productFront__content__card__listGroup__list:nth-child(3n){margin-right:0}.ri-productFront__content__card__listGroup__list:nth-child(3n+1){margin-left:0}@media (max-width:1199.98px){.ri-productFront__content__card__listGroup__list{width:calc(33.33% - 26.666px);margin-left:20px;margin-right:20px;padding:20px}}@media (max-width:767.98px){.ri-productFront__content__card__listGroup__list{padding:20px;margin-left:0;margin-right:0;width:100%;margin-bottom:20px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/productDuring.vue?vue&type=template&id=3d90cc07&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-layout"},[_vm._ssrNode("<div class=\"container ri-container\" data-v-3d90cc07>","</div>",[_c('ClassTitle',{attrs:{"class-title":_vm.ClassTitle,"language-title":_vm.LanguageTitle,"single-data":_vm.LanguageData}}),_vm._ssrNode(" "),_c('client-only',[_c('div',{staticClass:"ri-productFront",attrs:{"data-aos":"fade-up"}},[_c('div',{staticClass:"ri-productFront__content"},[_c('h2',{staticClass:"ri-subTitle ri-subTitle--content"},[_vm._v("\n            "+_vm._s(_vm.LanguageData.Name)+"\n          ")]),_vm._v(" "),_c('div',[(_vm.SingleData.EnableResponseContent === 1)?[_c('div',{staticClass:"d-none d-lg-block ck-content",domProps:{"innerHTML":_vm._s(_vm.LanguageData.ResponseData.Web.Content)}}),_vm._v(" "),_c('div',{staticClass:"d-block d-lg-none ck-content",domProps:{"innerHTML":_vm._s(_vm.LanguageData.ResponseData.Mobile.Content)}})]:(_vm.SingleData.EnableResponseContent === 0)?[_c('div',{staticClass:"ck-content",domProps:{"innerHTML":_vm._s(_vm.LanguageData.Content)}})]:_vm._e()],2)])])]),_vm._ssrNode(" <div class=\"d-none ri-productFront\" data-v-3d90cc07><div class=\"ri-productFront__content\" data-v-3d90cc07><h2 class=\"ri-subTitle ri-subTitle--content\" data-v-3d90cc07>"+_vm._ssrEscape("\n          "+_vm._s(_vm.LanguageData.Name)+"\n        ")+"</h2> <div data-v-3d90cc07>"+((_vm.SingleData.EnableResponseContent === 1)?("<div class=\"d-none d-lg-block ck-content\" data-v-3d90cc07>"+(_vm._s(_vm.LanguageData.ResponseData.Web.Content))+"</div> <div class=\"d-block d-lg-none ck-content\" data-v-3d90cc07>"+(_vm._s(_vm.LanguageData.ResponseData.Mobile.Content))+"</div>"):(_vm.SingleData.EnableResponseContent === 0)?("<div class=\"ck-content\" data-v-3d90cc07>"+(_vm._s(_vm.LanguageData.Content))+"</div>"):"<!---->")+"</div></div></div>")],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/productDuring.vue?vue&type=template&id=3d90cc07&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/productDuring.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var productDuringvue_type_script_lang_js_ = ({
  async asyncData({
    $axios,
    redirect,
    app
  }) {
    let LanguageTitle, LanguageData;
    const ClassTitle = await $axios.post('/api/category', {
      Slug: 'product'
    }).then(({
      data
    }) => {
      return data.Data;
    }).catch(error => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(error));
      redirect('/error');
    });
    const SingleData = await $axios.get('/api/article/detail/productDuring').then(({
      data
    }) => {
      if (data.Data === null) {
        return redirect('/error');
      }

      return data.Data;
    }).catch(error => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(error));
      redirect('/error');
    });

    if (app.i18n.locale && ClassTitle.LanguageData) {
      LanguageTitle = ClassTitle.LanguageData[`${app.i18n.locale}`];
    } else {
      LanguageTitle = ClassTitle;
    }

    if (app.i18n.locale && SingleData.LanguageData) {
      LanguageData = SingleData.LanguageData[`${app.i18n.locale}`];
    } else {
      LanguageData = SingleData;
    }

    return {
      ClassTitle,
      LanguageTitle,
      LanguageData,
      SingleData
    };
  },

  data() {
    return {
      Meta: {
        Title: 'P-Waver 國家地震中心'
      }
    };
  },

  head() {
    return {
      title: `${this.LanguageData.Name} | ${this.Meta.Title}`,
      meta: [{
        hid: 'description',
        name: 'description',
        content: this.LanguageTitle.SEO.Description
      }, {
        property: 'og:description',
        content: this.LanguageTitle.SEO.Description
      }, {
        name: 'keywords',
        content: this.LanguageTitle.SEO.Keywords
      }, {
        property: 'og:title',
        content: `${this.LanguageTitle.Name} | ${this.Meta.Title}`
      }, {
        property: 'og:image',
        content: `${this.LanguageTitle.Cover}`
      }, {
        property: 'og:type',
        content: 'article'
      }]
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.Meta = this.DetermineLanguage(this.Meta);
    }

  },

  mounted() {
    this.Meta = this.DetermineLanguage(this.$store.state.Meta);
  },

  methods: {
    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/productDuring.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_productDuringvue_type_script_lang_js_ = (productDuringvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./pages/productDuring.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(141)
if (style0.__inject__) style0.__inject__(context)
var style1 = __webpack_require__(143)
if (style1.__inject__) style1.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_productDuringvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "3d90cc07",
  "0579441f"
  
)

/* harmony default export */ var productDuring = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {ClassTitle: __webpack_require__(69).default})


/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=template&id=458214ec&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container ri-container"},[_vm._ssrNode("<div class=\"d-none d-md-block ri-breadCrumb\">","</div>",[(_vm.classTitle.Slug === 'threeD')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ThreeDSpace')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'product')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ProductAndServices')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'download')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#Download')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):_c('nuxt-link',{attrs:{"to":_vm.localePath(("/" + (_vm.classTitle.Slug)))}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]),_vm._ssrNode(" "),_c('b-icon',{attrs:{"icon":"chevron-right","aria-hidden":"true"}}),_vm._ssrNode(" "),_c('nuxt-link',{attrs:{"to":""}},[_vm._v("\n      "+_vm._s(_vm.singleData.Name)+"\n    ")])],2),_vm._ssrNode(" <div class=\"d-block d-md-none ri-breadCrumb\"><h2 class=\"ri-mainTitle\"><span>"+_vm._ssrEscape(_vm._s(_vm.classTitle.Name))+"</span></h2></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=template&id=458214ec&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ClassTitlevue_type_script_lang_js_ = ({
  props: {
    classTitle: {
      type: Object,
      default: () => ({})
    },
    languageTitle: {
      type: Object,
      default: () => ({})
    },
    singleData: {
      type: Object,
      default: null
    }
  }
});
// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ClassTitlevue_type_script_lang_js_ = (ClassTitlevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/ClassTitle.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ClassTitlevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "36d007cd"
  
)

/* harmony default export */ var ClassTitle = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=productDuring.js.map
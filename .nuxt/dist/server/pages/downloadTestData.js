exports.ids = [24,11];
exports.modules = {

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_downloadTestData_vue_vue_type_style_index_0_id_45792908_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(94);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_downloadTestData_vue_vue_type_style_index_0_id_45792908_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_downloadTestData_vue_vue_type_style_index_0_id_45792908_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_downloadTestData_vue_vue_type_style_index_0_id_45792908_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_downloadTestData_vue_vue_type_style_index_0_id_45792908_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 130:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ri-download[data-v-45792908]{margin-bottom:100px}.ri-download>ul[data-v-45792908]{list-style:none;padding-left:0;margin-bottom:0}@media (min-width:1200px) and (max-width:1439.98px){.ri-download[data-v-45792908]{margin-bottom:80px}}@media (max-width:1199.98px){.ri-download[data-v-45792908]{margin-bottom:60px}}.ri-download__card__list[data-v-45792908]{margin-bottom:20px;border:1px solid #3d3d3d;padding:20px 40px;display:flex;justify-content:space-between;align-items:center}.ri-download__card__list .ri-subTitle[data-v-45792908]{margin-bottom:0;padding-bottom:0;border-bottom:0}.ri-download__card__list[data-v-45792908]:hover{border:1px solid #00b3b6;color:#00b3b6}.ri-download__card__list:hover .ri-button[data-v-45792908]{color:#fff;border:1px solid #00b3b6;background-color:#00b3b6}@media (max-width:767.98px){.ri-download__card__list[data-v-45792908]{display:block;text-align:center}.ri-download__card__list .ri-subTitle[data-v-45792908]{margin-bottom:20px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/downloadTestData.vue?vue&type=template&id=45792908&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ri-layout"},[_vm._ssrNode("<div class=\"container ri-container\" data-v-45792908>","</div>",[_c('ClassTitle',{attrs:{"class-title":_vm.ClassTitle,"language-title":_vm.LanguageTitle,"single-data":_vm.$t('download.downloadTestData')}}),_vm._ssrNode(" "),_c('client-only',[_c('div',{staticClass:"ri-download",attrs:{"data-aos":"fade-up"}},[_c('h2',{staticClass:"ri-subTitle ri-subTitle--content"},[_vm._v("\n          "+_vm._s(_vm.$t('download.downloadTestData.name'))+"\n        ")]),_vm._v(" "),_c('ul',_vm._l((_vm.LanguageData),function(listItem){return _c('li',{key:listItem.Name,staticClass:"ri-download__card__list"},[_c('h3',{staticClass:"ri-subTitle"},[_vm._v("\n              "+_vm._s(listItem.Name)+"\n            ")]),_vm._v(" "),_c('button',{staticClass:"ri-button",on:{"click":function($event){return _vm.Download(listItem.Attachment)}}},[_vm._v("\n              "+_vm._s(_vm.$t('download.button'))+"\n            ")])])}),0),_vm._v(" "),_c('b-pagination-nav',{staticClass:"ri-pagination",attrs:{"pills":"","base-url":"#","next-text":_vm.page.next,"prev-text":_vm.page.prev,"first-number":"","last-number":"","number-of-pages":_vm.Pagination.TotalPages,"per-page":_vm.Pagination.PerPage},on:{"input":_vm.ChangePage},model:{value:(_vm.Pagination.Page),callback:function ($$v) {_vm.$set(_vm.Pagination, "Page", $$v)},expression:"Pagination.Page"}})],1)]),_vm._ssrNode(" "),_vm._ssrNode("<template data-v-45792908>","</template>",[_vm._ssrNode("<div class=\"d-none ri-download\" data-v-45792908>","</div>",[_vm._ssrNode("<h2 class=\"ri-subTitle ri-subTitle--content\" data-v-45792908>"+_vm._ssrEscape("\n          "+_vm._s(_vm.$t('download.downloadTestData.name'))+"\n        ")+"</h2> <ul data-v-45792908>"+(_vm._ssrList((_vm.LanguageData),function(listItem){return ("<li class=\"ri-download__card__list\" data-v-45792908><h3 class=\"ri-subTitle\" data-v-45792908>"+_vm._ssrEscape("\n              "+_vm._s(listItem.Name)+"\n            ")+"</h3> <button class=\"ri-button\" data-v-45792908>"+_vm._ssrEscape("\n              "+_vm._s(_vm.$t('download.button'))+"\n            ")+"</button></li>")}))+"</ul> "),_c('b-pagination-nav',{staticClass:"ri-pagination",attrs:{"pills":"","base-url":"#","next-text":_vm.page.next,"prev-text":_vm.page.prev,"first-number":"","last-number":"","number-of-pages":_vm.Pagination.TotalPages,"per-page":_vm.Pagination.PerPage},on:{"input":_vm.ChangePage},model:{value:(_vm.Pagination.Page),callback:function ($$v) {_vm.$set(_vm.Pagination, "Page", $$v)},expression:"Pagination.Page"}})],2)])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/downloadTestData.vue?vue&type=template&id=45792908&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/downloadTestData.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var downloadTestDatavue_type_script_lang_js_ = ({
  name: 'DownloadTestData',

  async asyncData({
    route,
    $axios,
    redirect,
    app
  }) {
    let LanguageTitle, LanguageData;
    const ClassTitle = await $axios.post('/api/category', {
      Slug: 'download'
    }).then(({
      data
    }) => {
      return data.Data;
    }).catch(error => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(error));
      redirect('/error');
    });
    const List = await $axios.post('/api/article/list', {
      Categories: ['download'],
      Tags: '檢測數據報告',
      Page: route.query.page ? parseInt(route.query.page) : 1,
      PerPage: route.query.perPage ? parseInt(route.query.perPage) : 10,
      Order: route.query.order ? route.query.order : 'sort_asc'
    }).then(({
      data
    }) => {
      return { ...data
      };
    }).catch(error => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(error));
      redirect('/error');
    });

    if (app.i18n.locale && ClassTitle.LanguageData) {
      LanguageTitle = ClassTitle.LanguageData[`${app.i18n.locale}`];
    } else {
      LanguageTitle = ClassTitle;
    }

    if (app.i18n.locale) {
      LanguageData = List.Data.map(item => {
        return item.LanguageData[`${app.i18n.locale}`];
      });
    } else {
      LanguageData = List;
    }

    return {
      ClassTitle,
      LanguageTitle,
      LanguageData,
      ...List
    };
  },

  data() {
    return {
      downLoad: '下載',
      page: {
        prev: '上一頁',
        next: '下一頁'
      },
      Meta: {
        Title: 'P-Waver 國家地震中心'
      }
    };
  },

  head() {
    return {
      title: `${this.$t('download.downloadTestData.name')} | ${this.Meta.Title}`,
      meta: [{
        hid: 'description',
        name: 'description',
        content: this.LanguageTitle.SEO.Description
      }, {
        property: 'og:description',
        content: this.LanguageTitle.SEO.Description
      }, {
        name: 'keywords',
        content: this.LanguageTitle.SEO.Keywords
      }, {
        property: 'og:title',
        content: `${this.LanguageTitle.Name} | ${this.Meta.Title}`
      }, {
        property: 'og:image',
        content: `${this.LanguageTitle.Cover}`
      }]
    };
  },

  watch: {
    '$i18n.locale'(newVal) {
      this.Meta = this.DetermineLanguage(this.Meta);
    }

  },

  mounted() {
    switch (this.$i18n.locale) {
      case 'ja-jp':
        this.page = {
          prev: '前へ',
          next: '次へ'
        };
        break;

      case 'en-us':
        this.page = {
          prev: 'prev',
          next: 'next'
        };
        break;

      default:
        this.page = {
          prev: '上一頁',
          next: '下一頁'
        };
    }

    this.Meta = this.DetermineLanguage(this.$store.state.Meta);
  },

  methods: {
    ChangePage() {
      this.$router.push({
        name: this.$route.name,
        query: {
          page: this.Pagination.Page,
          perPage: this.Pagination.PerPage,
          order: 'sort_asc'
        }
      });
      return this.$axios.post('/api/article/list', {
        Categories: ['menu'],
        Tags: '檢測數據報告',
        Page: parseInt(this.Pagination.Page),
        PerPage: parseInt(this.Pagination.PerPage),
        Order: 'sort_asc'
      }).then(({
        data
      }) => {
        this.Data = data.Data;
      });
    },

    Download(attachment) {
      window.open(attachment, 'Download Manual');
    },

    DetermineLanguage(obj) {
      if (this.$i18n.locale && obj.LanguageData) {
        return obj.LanguageData[`${this.$i18n.locale}`];
      } else {
        return obj;
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/downloadTestData.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_downloadTestDatavue_type_script_lang_js_ = (downloadTestDatavue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./pages/downloadTestData.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(129)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_downloadTestDatavue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "45792908",
  "1efd73c5"
  
)

/* harmony default export */ var downloadTestData = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {ClassTitle: __webpack_require__(69).default})


/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=template&id=458214ec&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container ri-container"},[_vm._ssrNode("<div class=\"d-none d-md-block ri-breadCrumb\">","</div>",[(_vm.classTitle.Slug === 'threeD')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ThreeDSpace')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'product')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#ProductAndServices')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):(_vm.classTitle.Slug === 'download')?_c('nuxt-link',{attrs:{"to":_vm.localePath('/#Download')}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]):_c('nuxt-link',{attrs:{"to":_vm.localePath(("/" + (_vm.classTitle.Slug)))}},[_vm._v("\n      "+_vm._s(_vm.languageTitle.Name)+"\n    ")]),_vm._ssrNode(" "),_c('b-icon',{attrs:{"icon":"chevron-right","aria-hidden":"true"}}),_vm._ssrNode(" "),_c('nuxt-link',{attrs:{"to":""}},[_vm._v("\n      "+_vm._s(_vm.singleData.Name)+"\n    ")])],2),_vm._ssrNode(" <div class=\"d-block d-md-none ri-breadCrumb\"><h2 class=\"ri-mainTitle\"><span>"+_vm._ssrEscape(_vm._s(_vm.classTitle.Name))+"</span></h2></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=template&id=458214ec&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ClassTitle.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ClassTitlevue_type_script_lang_js_ = ({
  props: {
    classTitle: {
      type: Object,
      default: () => ({})
    },
    languageTitle: {
      type: Object,
      default: () => ({})
    },
    singleData: {
      type: Object,
      default: null
    }
  }
});
// CONCATENATED MODULE: ./components/ClassTitle.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ClassTitlevue_type_script_lang_js_ = (ClassTitlevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(9);

// CONCATENATED MODULE: ./components/ClassTitle.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ClassTitlevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "36d007cd"
  
)

/* harmony default export */ var ClassTitle = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 94:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(130);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("491bbd34", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=downloadTestData.js.map
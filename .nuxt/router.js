import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _0f165a12 = () => interopDefault(import('..\\pages\\aboutUS.vue' /* webpackChunkName: "pages/aboutUS" */))
const _5dd94672 = () => interopDefault(import('..\\pages\\contact.vue' /* webpackChunkName: "pages/contact" */))
const _09d0b574 = () => interopDefault(import('..\\pages\\downloadDetect.vue' /* webpackChunkName: "pages/downloadDetect" */))
const _0f7598ff = () => interopDefault(import('..\\pages\\downloadSystemMenu.vue' /* webpackChunkName: "pages/downloadSystemMenu" */))
const _041d0f6d = () => interopDefault(import('..\\pages\\downloadTestData.vue' /* webpackChunkName: "pages/downloadTestData" */))
const _71041f8e = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))
const _7fa6fe74 = () => interopDefault(import('..\\pages\\news\\index.vue' /* webpackChunkName: "pages/news/index" */))
const _c38894d4 = () => interopDefault(import('..\\pages\\productAfter.vue' /* webpackChunkName: "pages/productAfter" */))
const _22579896 = () => interopDefault(import('..\\pages\\productBefore.vue' /* webpackChunkName: "pages/productBefore" */))
const _93c0ed12 = () => interopDefault(import('..\\pages\\productDuring.vue' /* webpackChunkName: "pages/productDuring" */))
const _30848b67 = () => interopDefault(import('..\\pages\\threeDSpace.vue' /* webpackChunkName: "pages/threeDSpace" */))
const _ab10d7f6 = () => interopDefault(import('..\\pages\\threeDSVG.vue' /* webpackChunkName: "pages/threeDSVG" */))
const _cdb4a616 = () => interopDefault(import('..\\pages\\threeDWeb.vue' /* webpackChunkName: "pages/threeDWeb" */))
const _644ffcae = () => interopDefault(import('..\\pages\\news\\_id.vue' /* webpackChunkName: "pages/news/_id" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/aboutUS",
    component: _0f165a12,
    name: "aboutUS___zh-tw"
  }, {
    path: "/contact",
    component: _5dd94672,
    name: "contact___zh-tw"
  }, {
    path: "/downloadDetect",
    component: _09d0b574,
    name: "downloadDetect___zh-tw"
  }, {
    path: "/downloadSystemMenu",
    component: _0f7598ff,
    name: "downloadSystemMenu___zh-tw"
  }, {
    path: "/downloadTestData",
    component: _041d0f6d,
    name: "downloadTestData___zh-tw"
  }, {
    path: "/en-us",
    component: _71041f8e,
    name: "index___en-us"
  }, {
    path: "/ja-jp",
    component: _71041f8e,
    name: "index___ja-jp"
  }, {
    path: "/news",
    component: _7fa6fe74,
    name: "news___zh-tw"
  }, {
    path: "/productAfter",
    component: _c38894d4,
    name: "productAfter___zh-tw"
  }, {
    path: "/productBefore",
    component: _22579896,
    name: "productBefore___zh-tw"
  }, {
    path: "/productDuring",
    component: _93c0ed12,
    name: "productDuring___zh-tw"
  }, {
    path: "/threeDSpace",
    component: _30848b67,
    name: "threeDSpace___zh-tw"
  }, {
    path: "/threeDSVG",
    component: _ab10d7f6,
    name: "threeDSVG___zh-tw"
  }, {
    path: "/threeDWeb",
    component: _cdb4a616,
    name: "threeDWeb___zh-tw"
  }, {
    path: "/en-us/aboutUS",
    component: _0f165a12,
    name: "aboutUS___en-us"
  }, {
    path: "/en-us/contact",
    component: _5dd94672,
    name: "contact___en-us"
  }, {
    path: "/en-us/downloadDetect",
    component: _09d0b574,
    name: "downloadDetect___en-us"
  }, {
    path: "/en-us/downloadSystemMenu",
    component: _0f7598ff,
    name: "downloadSystemMenu___en-us"
  }, {
    path: "/en-us/downloadTestData",
    component: _041d0f6d,
    name: "downloadTestData___en-us"
  }, {
    path: "/en-us/news",
    component: _7fa6fe74,
    name: "news___en-us"
  }, {
    path: "/en-us/productAfter",
    component: _c38894d4,
    name: "productAfter___en-us"
  }, {
    path: "/en-us/productBefore",
    component: _22579896,
    name: "productBefore___en-us"
  }, {
    path: "/en-us/productDuring",
    component: _93c0ed12,
    name: "productDuring___en-us"
  }, {
    path: "/en-us/threeDSpace",
    component: _30848b67,
    name: "threeDSpace___en-us"
  }, {
    path: "/en-us/threeDSVG",
    component: _ab10d7f6,
    name: "threeDSVG___en-us"
  }, {
    path: "/en-us/threeDWeb",
    component: _cdb4a616,
    name: "threeDWeb___en-us"
  }, {
    path: "/ja-jp/aboutUS",
    component: _0f165a12,
    name: "aboutUS___ja-jp"
  }, {
    path: "/ja-jp/contact",
    component: _5dd94672,
    name: "contact___ja-jp"
  }, {
    path: "/ja-jp/downloadDetect",
    component: _09d0b574,
    name: "downloadDetect___ja-jp"
  }, {
    path: "/ja-jp/downloadSystemMenu",
    component: _0f7598ff,
    name: "downloadSystemMenu___ja-jp"
  }, {
    path: "/ja-jp/downloadTestData",
    component: _041d0f6d,
    name: "downloadTestData___ja-jp"
  }, {
    path: "/ja-jp/news",
    component: _7fa6fe74,
    name: "news___ja-jp"
  }, {
    path: "/ja-jp/productAfter",
    component: _c38894d4,
    name: "productAfter___ja-jp"
  }, {
    path: "/ja-jp/productBefore",
    component: _22579896,
    name: "productBefore___ja-jp"
  }, {
    path: "/ja-jp/productDuring",
    component: _93c0ed12,
    name: "productDuring___ja-jp"
  }, {
    path: "/ja-jp/threeDSpace",
    component: _30848b67,
    name: "threeDSpace___ja-jp"
  }, {
    path: "/ja-jp/threeDSVG",
    component: _ab10d7f6,
    name: "threeDSVG___ja-jp"
  }, {
    path: "/ja-jp/threeDWeb",
    component: _cdb4a616,
    name: "threeDWeb___ja-jp"
  }, {
    path: "/en-us/news/:id",
    component: _644ffcae,
    name: "news-id___en-us"
  }, {
    path: "/ja-jp/news/:id",
    component: _644ffcae,
    name: "news-id___ja-jp"
  }, {
    path: "/news/:id",
    component: _644ffcae,
    name: "news-id___zh-tw"
  }, {
    path: "/",
    component: _71041f8e,
    name: "index___zh-tw"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config.app && config.app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}

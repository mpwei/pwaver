import Vue from 'vue'

const components = {
  ClassTitle: () => import('../..\\components\\ClassTitle.vue' /* webpackChunkName: "components/class-title" */).then(c => c.default || c),
  Footer: () => import('../..\\components\\Footer.vue' /* webpackChunkName: "components/footer" */).then(c => c.default || c),
  Header: () => import('../..\\components\\Header.vue' /* webpackChunkName: "components/header" */).then(c => c.default || c),
  Loading: () => import('../..\\components\\Loading.vue' /* webpackChunkName: "components/loading" */).then(c => c.default || c),
  Logo: () => import('../..\\components\\Logo.vue' /* webpackChunkName: "components/logo" */).then(c => c.default || c),
  ScrollTop: () => import('../..\\components\\ScrollTop.vue' /* webpackChunkName: "components/scroll-top" */).then(c => c.default || c),
  ThreeDInner: () => import('../..\\components\\ThreeDInner.vue' /* webpackChunkName: "components/three-dinner" */).then(c => c.default || c),
  IndexAboutAndNews: () => import('../..\\components\\index\\AboutAndNews.vue' /* webpackChunkName: "components/index-about-and-news" */).then(c => c.default || c),
  IndexBanner: () => import('../..\\components\\index\\Banner.vue' /* webpackChunkName: "components/index-banner" */).then(c => c.default || c),
  IndexCustomer: () => import('../..\\components\\index\\Customer.vue' /* webpackChunkName: "components/index-customer" */).then(c => c.default || c),
  IndexDownload: () => import('../..\\components\\index\\Download.vue' /* webpackChunkName: "components/index-download" */).then(c => c.default || c),
  IndexFieldCase: () => import('../..\\components\\index\\FieldCase.vue' /* webpackChunkName: "components/index-field-case" */).then(c => c.default || c),
  IndexInformationData: () => import('../..\\components\\index\\InformationData.vue' /* webpackChunkName: "components/index-information-data" */).then(c => c.default || c),
  IndexProductAndServices: () => import('../..\\components\\index\\ProductAndServices.vue' /* webpackChunkName: "components/index-product-and-services" */).then(c => c.default || c),
  IndexThreeDSpace: () => import('../..\\components\\index\\ThreeDSpace.vue' /* webpackChunkName: "components/index-three-dspace" */).then(c => c.default || c)
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}

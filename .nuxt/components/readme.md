# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<ClassTitle>` | `<class-title>` (components/ClassTitle.vue)
- `<Footer>` | `<footer>` (components/Footer.vue)
- `<Header>` | `<header>` (components/Header.vue)
- `<Loading>` | `<loading>` (components/Loading.vue)
- `<Logo>` | `<logo>` (components/Logo.vue)
- `<ScrollTop>` | `<scroll-top>` (components/ScrollTop.vue)
- `<ThreeDInner>` | `<three-dinner>` (components/ThreeDInner.vue)
- `<IndexAboutAndNews>` | `<index-about-and-news>` (components/index/AboutAndNews.vue)
- `<IndexBanner>` | `<index-banner>` (components/index/Banner.vue)
- `<IndexCustomer>` | `<index-customer>` (components/index/Customer.vue)
- `<IndexDownload>` | `<index-download>` (components/index/Download.vue)
- `<IndexFieldCase>` | `<index-field-case>` (components/index/FieldCase.vue)
- `<IndexInformationData>` | `<index-information-data>` (components/index/InformationData.vue)
- `<IndexProductAndServices>` | `<index-product-and-services>` (components/index/ProductAndServices.vue)
- `<IndexThreeDSpace>` | `<index-three-dspace>` (components/index/ThreeDSpace.vue)

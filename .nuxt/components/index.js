export { default as ClassTitle } from '../..\\components\\ClassTitle.vue'
export { default as Footer } from '../..\\components\\Footer.vue'
export { default as Header } from '../..\\components\\Header.vue'
export { default as Loading } from '../..\\components\\Loading.vue'
export { default as Logo } from '../..\\components\\Logo.vue'
export { default as ScrollTop } from '../..\\components\\ScrollTop.vue'
export { default as ThreeDInner } from '../..\\components\\ThreeDInner.vue'
export { default as IndexAboutAndNews } from '../..\\components\\index\\AboutAndNews.vue'
export { default as IndexBanner } from '../..\\components\\index\\Banner.vue'
export { default as IndexCustomer } from '../..\\components\\index\\Customer.vue'
export { default as IndexDownload } from '../..\\components\\index\\Download.vue'
export { default as IndexFieldCase } from '../..\\components\\index\\FieldCase.vue'
export { default as IndexInformationData } from '../..\\components\\index\\InformationData.vue'
export { default as IndexProductAndServices } from '../..\\components\\index\\ProductAndServices.vue'
export { default as IndexThreeDSpace } from '../..\\components\\index\\ThreeDSpace.vue'

export const LazyClassTitle = import('../..\\components\\ClassTitle.vue' /* webpackChunkName: "components/class-title" */).then(c => c.default || c)
export const LazyFooter = import('../..\\components\\Footer.vue' /* webpackChunkName: "components/footer" */).then(c => c.default || c)
export const LazyHeader = import('../..\\components\\Header.vue' /* webpackChunkName: "components/header" */).then(c => c.default || c)
export const LazyLoading = import('../..\\components\\Loading.vue' /* webpackChunkName: "components/loading" */).then(c => c.default || c)
export const LazyLogo = import('../..\\components\\Logo.vue' /* webpackChunkName: "components/logo" */).then(c => c.default || c)
export const LazyScrollTop = import('../..\\components\\ScrollTop.vue' /* webpackChunkName: "components/scroll-top" */).then(c => c.default || c)
export const LazyThreeDInner = import('../..\\components\\ThreeDInner.vue' /* webpackChunkName: "components/three-dinner" */).then(c => c.default || c)
export const LazyIndexAboutAndNews = import('../..\\components\\index\\AboutAndNews.vue' /* webpackChunkName: "components/index-about-and-news" */).then(c => c.default || c)
export const LazyIndexBanner = import('../..\\components\\index\\Banner.vue' /* webpackChunkName: "components/index-banner" */).then(c => c.default || c)
export const LazyIndexCustomer = import('../..\\components\\index\\Customer.vue' /* webpackChunkName: "components/index-customer" */).then(c => c.default || c)
export const LazyIndexDownload = import('../..\\components\\index\\Download.vue' /* webpackChunkName: "components/index-download" */).then(c => c.default || c)
export const LazyIndexFieldCase = import('../..\\components\\index\\FieldCase.vue' /* webpackChunkName: "components/index-field-case" */).then(c => c.default || c)
export const LazyIndexInformationData = import('../..\\components\\index\\InformationData.vue' /* webpackChunkName: "components/index-information-data" */).then(c => c.default || c)
export const LazyIndexProductAndServices = import('../..\\components\\index\\ProductAndServices.vue' /* webpackChunkName: "components/index-product-and-services" */).then(c => c.default || c)
export const LazyIndexThreeDSpace = import('../..\\components\\index\\ThreeDSpace.vue' /* webpackChunkName: "components/index-three-dspace" */).then(c => c.default || c)

import crypto from 'crypto'
import { request } from '@/plugins/axios'

export const state = () => ({
  Contact: {},
  Meta: {},
  Social: [],
  GlobalArticleData: {}
})

export const actions = {
  nuxtServerInit ({ dispatch }, { $cookies }) {
    if (!$cookies.get('visitID')) {
      $cookies.set('visitID', crypto.randomBytes(16).toString('hex'), {
        maxAge: 60 * 60 * 24 * 7 // cookie保存一周
      })
    }
    return Promise.all([
      dispatch('LoadContact'),
      dispatch('LoadMeta'),
      dispatch('LoadSocial'),
      // dispatch('LoadGlobalArticleData', { categories: 'threeD', order: 'sort_asc' }),
      dispatch('LoadGlobalArticleData', { categories: 'aboutUS', order: 'sort_asc' }),
      dispatch('LoadGlobalArticleData', { categories: 'product', order: 'sort_asc' })
    ]).catch((error) => {
      console.log(error)
    })
  },
  // 載入據點
  LoadContact ({ commit }) {
    return request.get('/api/themes/contact').then(({ data }) => {
      commit('SetContact', data)
      return true
    }).catch((err) => {
      throw err
    })
  },
  // 載入SEO
  LoadMeta ({ commit }) {
    return request.get('/api/themes/meta').then(({ data }) => {
      commit('SetMeta', data)
    }).catch((err) => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(err))
      throw err
    })
  },
  // 載入社交
  LoadSocial ({ commit }) {
    return request.get('/api/themes/social').then(({ data }) => {
      commit('SetSocial', data)
    }).catch((err) => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(err))
      throw err
    })
  },
  LoadGlobalArticleData ({ commit }, payloads) {
    return request.post('/api/article/list', {
      Categories: payloads.categories,
      Order: payloads.order || 'nto'
    }, {}).then(({ data }) => {
      commit('SetGlobalArticleData', {
        ...data,
        Categories: (typeof payloads.categories === 'object' && Array.isArray(payloads.categories)) ? payloads.categories[0] : payloads.categories
      })
    }).catch((err) => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(err))
      throw err
    })
  }
}

export const mutations = {
  SetContact (state, payload) {
    state.Contact = payload
  },
  SetMeta (state, payload) {
    state.Meta = payload
  },
  SetSocial (state, payload) {
    state.Social = payload
  },
  SetGlobalArticleData (state, payload) {
    state.GlobalArticleData[payload.Categories] = payload.Data
  }
}

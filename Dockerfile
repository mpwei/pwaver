FROM node:12
ARG BASE_URL

WORKDIR /usr/src/app

ENV PORT 3000
ENV HOST 0.0.0.0
ENV BASE_URL ${BASE_URL}

COPY package.json ./
RUN npm install --production

COPY . .
EXPOSE 3000

CMD npm run start

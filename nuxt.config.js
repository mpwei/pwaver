export default {
  server: {
    port: 3000,
    host: (process.env.NODE_ENV === 'production' ? '0.0.0.0' : 'localhost'),
    timing: false
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'P-Waver',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'keywords', content: 'P-WAVER' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' },
      { hid: 'description', name: 'description', content: 'LEADER IN PRIVATE EARLY EARTHQUAKE WARNING SYSTEM' },
      { property: 'og:title', content: 'P-WAVER' },
      { property: 'og:description', content: 'LEADER IN PRIVATE EARLY EARTHQUAKE WARNING SYSTEM' },
      { property: 'og:url', content: 'https://pwaver.com/' },
      { property: 'og:type', content: 'website' },
      { property: 'og:site_name', content: 'P-WAVER' },
      { property: 'og:image', content: 'https://firebasestorage.googleapis.com/v0/b/p-waver.appspot.com/o/product%2Fpwaver-logo.jpg?alt=media&token=ed32181f-c400-4787-8640-c81d39485df0' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@300;400;500;700&display=swap' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    { src: '@/static/font-icom/style.css' },
    { src: '@/assets/sass/all.scss', lang: 'scss' }
  ],
  router: {
    extendRoutes (routes, resolve) {
    }
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/aos.js', mode: 'client' },
    { src: '~/plugins/vue-animatedCounter.js', mode: 'client' },
    { src: '~/plugins/vee-validate.js' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],
  bootstrapVue: {
    icons: true
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/dayjs',
    'cookie-universal-nuxt',
    // https://i18n.nuxtjs.org/
    'nuxt-i18n'
  ],
  serverMiddleware: [
    { path: '/api', handler: '~/server/app' }
  ],
  loading: {
    color: '#00B3B6',
    height: '3px'
  },
  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.NODE_ENV === 'production' ? process.env.BASE_URL : 'http://localhost:3000'
  },
  // i18n: https://i18n.nuxtjs.org/basic-usage
  i18n: {
    locales: [
      {
        code: 'zh-tw',
        file: 'zh-tw.js'
      },
      {
        code: 'en-us',
        file: 'en-us.js'
      },
      {
        code: 'ja-jp',
        file: 'ja-jp.js'
      }
    ],
    lazy: true,
    langDir: 'locales/',
    strategy: 'prefix_except_default',
    defaultLocale: 'zh-tw',
    vueI18n: {
      fallbackLocale: 'zh-tw'
    },
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'Locales',
      onlyOnRoot: true,
      alwaysRedirect: true
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['vee-validate/dist/rules'],
    babel: {
      presets ({ envName }) {
        return [
          ['@nuxt/babel-preset-app', {
            corejs: {
              version: 3,
              proposals: true
            }
          }]
        ]
      }
    },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000
      }
    },
    splitChunks: {
      layouts: true,
      pages: true,
      commons: true
    }
  }
}
